#include <canopy/io/net/Endpoint.h>
#include <iostream>

int main(int argc, const char** argv)
{
   for (int i = 1; i < argc; ++i) {
      auto ep = ::canopy::io::net::Endpoint::createEndpoint(argv[1]);
      if (i==1) {
         ::std::cout << "#type\tdomain\tprotocol\taddress" << ::std::endl;
      }
      for (auto addr : ep) {
         ::std::cout << addr.type() << '\t'<< addr.domain().id() << '\t' << addr.protocol().id() << '\t' << addr << ::std::endl;
      }
   }
   return 0;
}
