#include <canopy/common.h>


#if CANOPY_COMPILER != CANOPY_GCC
   size_t countOnes(::std::uint32_t x)
   {
      size_t n = 0;
      for (size_t i = 0; i < 32; ++i) {
         if ((x >> i & 1)) {
            ++n;
         }
      }
      return n;
   }
   size_t countOnes(::std::uint64_t x)
   {
      size_t n = 0;
      for (size_t i = 0; i < 64; ++i) {
         if ((x >> i & 1)) {
            ++n;
         }
      }
      return n;
   }
#endif
