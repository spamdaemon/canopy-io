#ifndef _CANOPY_IO_PIPE_H
#define _CANOPY_IO_PIPE_H

#ifndef _CANOPY_IO_IOEXCEPTION_H
#include <canopy/io/IOException.h>
#endif

#ifndef _CANOPY_IO_NATIVEFILEDESCRIPTOR_H
#include <canopy/io/NativeFileDescriptor.h>
#endif

#include <memory>

namespace canopy {
   namespace io {

      /**
       * A unidirectional pipe between two file descriptors.
       * @note This class is not threadsafe!
       */
      class Pipe
      {
            /** A descriptor. */
         public:
            typedef ::std::shared_ptr< NativeFileDescriptor> Descriptor;

            /** The default buffer size */
         public:
            static const size_t BUFFER_SIZE = 2048;

            /** Delete copy constructor */
         public:
            Pipe(const Pipe&) = delete;

            /** Delete copy operator */
         public:
            Pipe& operator=(const Pipe&) = delete;

            /**
             * A default constructor.
             */
         public:
            Pipe();

            /**
             * Create a new pipe.
             * @param r a reader descriptor
             * @param w a writer descriptor
             * @param size the size of the pipe's buffer (if 0, then a default is used)
             */
         public:
            Pipe(Descriptor r, Descriptor w, size_t size = 0);

            /**
             * A copy constructor.
             * @param src a pipe
             */
         public:
            Pipe(Pipe&& pipe);

            /**
             * The destructor
             */
         public:
            virtual ~Pipe() throw();

            /**
             * A copy constructor.
             * @param src a pipe
             */
         public:
            Pipe& operator=(Pipe&& pipe);

            /**
             * Close both ends of this pipe.
             */
         public:
            void close();

            /**
             * Close the reader end of this pipe.
             */
         public:
            virtual void closeReader();

            /**
             * Close the writer end of this pipe.
             */
         public:
            virtual void closeWriter();

            /**
             * Get the input descriptor for this pipe.
             * @return the input descriptor
             */
         public:
            inline Descriptor reader() const
            {
               return _reader;
            }

            /**
             * Get the output descriptor for this pipe.
             * @return the output descriptor
             */
         public:
            inline Descriptor writer() const
            {
               return _writer;
            }

            /**
             * Determine events that can be used with a selector.
             * @param revents the new input events
             * @param wevents the new output events
             * @return true if there are some events that can be selected on
             */
         public:
            bool determineEvents(IOEvents& revents, IOEvents& wevents) const;

            /*
             * Transfer data from the reading end to the writing end in a non-blocking fashion.
             * If the wevents indicates an error, then this pipe will be closed; if revents
             * indicates only an error, then the input side of this pipe is closed.
             * @param revents the events that are available on the reader descriptor
             * @param wevents the events that are available on the writer descriptor
             * @return the number of bytes written or read (0 if no IO operation occurred)
             */
         public:
            size_t transfer(const IOEvents& revents = IOEvents(IOEvents::READ),
                  const IOEvents& wEvents = IOEvents(IOEvents::WRITE));

            /*
             * Discard the content of this pipe.
             */
         public:
            void clear();

            /**
             * Wrap the queue if possible.
             */
         private:
            void wrap();

            /** The reader */
         private:
            Descriptor _reader;

            /** The reader */
         private:
            Descriptor _writer;

            /** True if the output has been closed */
         private:
            bool _outputClosed;

            /** True if the input has been closed */
         private:
            bool _inputClosed;

            /** An internal buffer */
         private:
            ::std::unique_ptr< char[]> _buffer;

            /** The size of the buffer */
         private:
            size_t _bufferSize;

            /** The number of bytes that have been read */
         private:
            size_t _nRead;

            /** The number of bytes that have been written. */
         private:
            size_t _nWritten;
      };
   }
}

#endif
