#ifndef _CANOPY_IO_IOEXCEPTION_H
#define _CANOPY_IO_IOEXCEPTION_H

#include <stdexcept>
#include <string>

namespace canopy {
   namespace io {

      /**
       * This exception may thrown by operations that work
       * no connected sockets.
       */
      class IOException : public ::std::runtime_error
      {
         private:
            IOException() = delete;

            /**
             * Create an exception with the specified error string.
             * The exception code is set to OTHER.
             * @param msg an error message
             */
         public:
            IOException(  ::std::string msg) ;

            /** Destructor */
         public:
            ~IOException() ;
      };
   }
}

#endif
