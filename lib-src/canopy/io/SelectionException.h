#ifndef _CANOPY_IO_SELECTIONEXCEPTION_H
#define _CANOPY_IO_SELECTIONEXCEPTION_H

#ifndef _CANOPY_IO_IOEXCEPTION_H
#include <canopy/io/IOException.h>
#endif

namespace canopy {
   namespace io {

      /**
       * This exception may thrown by operations that work
       * no connected selections.
       */
      class SelectionException : public IOException
      {
         private:
            SelectionException();

            /**
             * The various exception codes.
             */
         public:
            enum ExceptionCode
            {
               CONCURRENT_MODIFICATION, /// a selection is already in progress
               OTHER /// other error codes
            };

            /**
             * Create an exception with the specified code.
             * @param code an exception code
             */
         public:
            SelectionException(ExceptionCode code) ;

            /**
             * Create an exception with the specified error string.
             * The exception code is set to OTHER.
             * @param msg an error message
             */
         public:
            SelectionException(  ::std::string  msg) ;

            /** Destructor */
         public:
            ~SelectionException() ;

            /**
             * Get the exception code.
             * @return exception code.
             */
         public:
            inline ExceptionCode getCode() const 
            {
               return _code;
            }

            /** The error code. */
         private:
            ExceptionCode _code;
      };
   }
}

#endif
