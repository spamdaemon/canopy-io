#ifndef _CANOPY_IO_H
#define _CANOPY_IO_H

#include <chrono>
#include <string>

namespace canopy {
   namespace io {

      /** A timeout for IO operations is always in nanoseconds */
      typedef ::std::chrono::nanoseconds Timeout;

      /** A timeout that is blocking */
      constexpr Timeout BLOCKING_TIMEOUT(-1);

      /** A timeout that is non-blocking */
      constexpr Timeout NON_BLOCKING_TIMEOUT(0);

      /**
       * Make a timeout value from an arbitrary duration.
       * @param duration a duration
       * @return a timeout
       */
      template<class X, class Y>
      inline Timeout makeTimeout(const ::std::chrono::duration< X, Y>& duration) 
      {
         return ::std::chrono::duration_cast< Timeout>(duration);
      }

      /**
       * Determine if the timeout value denotes non-blocking.
       * @param tm a timeout
       * @return true if tm==0
       */
      inline bool isNonBlockingTimeout(const Timeout& tm) 
      {
         return tm == Timeout::zero();
      }

      /**
       * Determine if the timeout value denotes infinite blocking.
       * @param tm a timeout
       * @return true if tm<0
       */
      inline bool isBlockingTimeout(const Timeout& tm) 
      {
         return tm < Timeout::zero();
      }

      /**
       * Determine if the timeout is finite, but not immediate.
       * @param tm a timeout
       * @return true if tm>0
       */
      inline bool isFiniteTimeout(const Timeout& tm) 
      {
         return tm > Timeout::zero();
      }

   }
}

#endif
