#include <canopy/io/signals/Signal.h>

namespace canopy {
   namespace io {
      namespace signals {

         Signal::Signal(SignalID id, ProcessID pid)
               : _signal(id), _process(pid)
         {
         }

         Signal::Signal(SignalID id)
               : _signal(id), _process(0)
         {
         }

         Signal::~Signal()
         {
         }
      }
   }
}

