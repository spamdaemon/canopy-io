#include <canopy/io/signals/NativeSignalDescriptor.h>
#include <canopy/common.h>
#include <canopy/io/FileDescriptor.h>
#include <canopy/io/IOException.h>
#include <canopy/os/ErrorStatus.h>

#include <iostream>
#include <cassert>

#include <sys/signalfd.h>
#include <signal.h>

using namespace ::std;

namespace canopy {
   namespace io {
      namespace signals {
         namespace {
            ::std::unique_ptr< Signal> createSignal(const signalfd_siginfo& info)
            {
               ::std::unique_ptr< Signal> res;
               res.reset(new Signal(info.ssi_signo, info.ssi_pid));
               return res;
            }
         }

         NativeSignalDescriptor::NativeSignalDescriptor()

               : _fd(new FileDescriptor())
         {
         }

         NativeSignalDescriptor::NativeSignalDescriptor(int fd)

               : _fd(new FileDescriptor(fd, true))
         {
         }

         NativeSignalDescriptor::~NativeSignalDescriptor()
         {
         }

         ::std::unique_ptr< NativeSignalDescriptor> NativeSignalDescriptor::create(
               const ::std::vector< Signal::SignalID>& signals)
         {
            sigset_t mask;
            sigemptyset(&mask);
            for (auto id : signals) {
               sigaddset(&mask, id);
            }

            // block the signal
            sigprocmask(SIG_BLOCK, &mask, nullptr);

            auto fd = signalfd(-1, &mask, SFD_CLOEXEC);
            if (fd < 0) {
               throw IOException(canopy::os::ErrorStatus::getFormattedMessage("SignalDescriptor::create"));
            }
            ::std::unique_ptr< NativeSignalDescriptor> sd(new NativeSignalDescriptor(fd));
            return sd;
         }

         ::std::unique_ptr< NativeSignalDescriptor> NativeSignalDescriptor::create(Signal::SignalID s)
         {
            ::std::vector< Signal::SignalID> signals;
            signals.push_back(s);
            return create(signals);
         }

         ::std::unique_ptr< NativeSignalDescriptor> NativeSignalDescriptor::create()
         {
            ::std::vector< Signal::SignalID> sigs;
            return create(sigs);
         }

         NativeSignalDescriptor::Descriptor NativeSignalDescriptor::descriptor() const
         {
            return _fd->descriptor();
         }

         bool NativeSignalDescriptor::invalid() const
         {
            return descriptor() == -1;
         }

         void NativeSignalDescriptor::close()
         {
            _fd->close();
         }

         bool NativeSignalDescriptor::isClosed() const
         {
            return _fd->isClosed();
         }

         IOEvents NativeSignalDescriptor::block(const IOEvents& events, const Timeout& timeout) const
         {
            return _fd->block(events, timeout);
         }

         ssize_t NativeSignalDescriptor::peek(char*, size_t, const Timeout&) const
         {
            throw IOException("Unsupported method");
         }

         ssize_t NativeSignalDescriptor::read(char*, size_t, const Timeout&)
         {
            throw IOException("Unsupported method");
         }

         size_t NativeSignalDescriptor::write(const char*, size_t, const Timeout&)
         {
            throw IOException("Unsupported method");
         }

         void NativeSignalDescriptor::setBlockingEnabled(bool enabled)
         {
            _fd->setBlockingEnabled(enabled);
         }

         bool NativeSignalDescriptor::isBlockingEnabled() const
         {
            return _fd->isBlockingEnabled();
         }

         ::std::unique_ptr< Signal> NativeSignalDescriptor::peek(const Timeout& timeout) const
         {
            signalfd_siginfo info;
            auto nRead = _fd->peek(reinterpret_cast< char*>(&info), sizeof(signalfd_siginfo), timeout);
            if (nRead == sizeof(signalfd_siginfo)) {
               return createSignal(info);
            }
            else if (nRead < 0) {
               // looks like the descriptor has been closed
               throw IOException("Signal Descriptor closed");
            }
            return nullptr;
         }

         ::std::unique_ptr< Signal> NativeSignalDescriptor::read(const Timeout& timeout)
         {
            signalfd_siginfo info;
            auto nRead = _fd->read(reinterpret_cast< char*>(&info), sizeof(signalfd_siginfo), timeout);
            if (nRead == sizeof(signalfd_siginfo)) {
               return createSignal(info);
            }
            else if (nRead < 0) {
               // looks like the descriptor has been closed
               throw IOException("Signal Descriptor closed");
            }
            return nullptr;
         }
      }
   }
}
