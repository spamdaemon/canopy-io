#ifndef _CANOPY_IO_SIGNALS_SIGNALDESCRIPTORIMPL_H
#define _CANOPY_IO_SIGNALS_SIGNALDESCRIPTORIMPL_H

#ifndef _CANOPY_IO_SIGNALS_SIGNAL_H
#include <canopy/io/signals/Signal.h>
#endif

#ifndef _CANOPY_IO_NATIVEFILEDESCRIPTOR_H_H
#include <canopy/io/NativeFileDescriptor.h>
#endif

#include <memory>
#include <atomic>
#include <vector>

namespace canopy {
   namespace io {
      class FileDescriptor;
      namespace signals {

         /**
          * This is a simple native file descriptor
          */
         class NativeSignalDescriptor : public ::canopy::io::NativeFileDescriptor
         {
               NativeSignalDescriptor(const NativeSignalDescriptor&) = delete;
               NativeSignalDescriptor&operator=(const NativeSignalDescriptor&) = delete;

               /**
                * Create an invalid file descriptor
                */
            public:
               NativeSignalDescriptor();

               /**
                * Create an invalid file descriptor
                * @param fd the signal descriptor
                */
            private:
               NativeSignalDescriptor(Descriptor fd);

               /**
                * Destroy this socket reference. If this is the last
                * reference to the socket, then it is destroyed.
                * If the socket cannot be destroyed properly, then an error
                * message is printed to cerr
                */
            public:
               ~NativeSignalDescriptor();

               /**
                * Create a native signal descriptor.
                * @return a descriptor
                */
            public:
               static ::std::unique_ptr< NativeSignalDescriptor> create();

               /**
                * Create a native signal descriptor.
                * @param signals a list of signals to block
                * @return a descriptor
                */
            public:
               static ::std::unique_ptr< NativeSignalDescriptor> create(
                     const ::std::vector< Signal::SignalID>& signals);

               /**
                * Create a native signal descriptor.
                * @param s a signal to block
                * @return a descriptor
                */
            public:
               static ::std::unique_ptr< NativeSignalDescriptor> create(Signal::SignalID s);

               /**
                * Get the id for this socket.
                * @note the id is not constant for the lifetime of this socket.
                * Calling close() on the socket will change the id.
                * @return the unique id.
                */
            public:
               inline ::std::int32_t id() const
               {
                  // use the socket descriptor as an id
                  return descriptor();
               }

               /**
                * Get the descriptor
                * @return the descriptor
                */
            public:
               Descriptor descriptor() const;

               /**
                * Test if this is an invalid socket.
                * @return true if this socket is <em>invalid</em>
                */
            public:
               bool invalid() const;

               /**
                * Close this socket.
                * @note this method will change the socket's id
                * @throws Exception if the socket could not be closed
                */
            public:
               void close();

               /**
                * Test if this socket has been closed. If this
                * method returns false, then this does not mean
                * that the next method invocation for this socket
                * will succeed.
                * @return true if this socket can no longer be used, false
                * if the socket currently not closed
                */
            public:
               bool isClosed() const;

            public:
               ::canopy::io::IOEvents block(const ::canopy::io::IOEvents& events,
                     const ::canopy::io::Timeout& timeout) const;

               /**
                * Peek at the buffer in the socket. This method works just like read, but does not
                * consume the data on the socket. This method is ideal for checking if a socket has been
                * closed in an orderly fashion, as this method will return 0 in that case.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to read
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t peek(char* buffer, size_t bufSize, const ::canopy::io::Timeout& timeout) const;

               /**
                * Read a number of bytes from this socket. This method blocks
                * unless the socket is a non-blocking socket or there is already
                * data to be read. This method must return 0 to indicate that the socket
                * buffer is full.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to read
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t read(char* buffer, size_t bufSize, const ::canopy::io::Timeout& timeout);

               /**
                * Unsupported method that always throws an IOException.
                * @param buffer ignored
                * @param bufSize ignored
                * @param block ignored
                * @return the number of bytes written to the socket or 0 if nothing can be written at the moment
                * @exception IOException if an error occurred during writing
                */
            public:
               size_t write(const char* buffer, size_t bufSize, const ::canopy::io::Timeout& timeout);

               /**
                * Peek the next signal.
                * @return the next signal or nullptr if the timeout expired
                */
            public:
               ::std::unique_ptr< Signal> peek(const ::canopy::io::Timeout& timeout) const;

               /**
                * Read the next signal.
                * @return the next signal or nullptr if the timeout expired
                */
            public:
               ::std::unique_ptr< Signal> read(const ::canopy::io::Timeout& timeout);

               /**
                * Set the socket to block on I/O operations.
                * @param enabled if true, then I/O operations may block
                * @exception IOException if an error occurred
                */
            public:
               void setBlockingEnabled(bool enabled);

               /**
                * Test if blocking I/O is enabled.
                * @return true if I/O calls may be block, false otherwise
                * @exception IOException if an error occurred
                */
            public:
               bool isBlockingEnabled() const;

               /** The descriptor itself */
            private:
               /** The file descriptor to use */
               ::std::unique_ptr< ::canopy::io::FileDescriptor> _fd;
         };
      }
   }
}
#endif
