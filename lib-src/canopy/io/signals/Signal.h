#ifndef _CANOPY_IO_SIGNALS_SIGNAL_H
#define _CANOPY_IO_SIGNALS_SIGNAL_H

#include <cstdint>
#include <signal.h>

namespace canopy {
   namespace io {
      namespace signals {

         /**
          * This class represents a received signal.
          */
         class Signal
         {
               /** The signal ID */
            public:
               typedef uint32_t SignalID;

               /** The process id */
            public:
               typedef uint32_t ProcessID;

               /**
                * Create a new signal.
                * @param id the signal
                */
            public:
               Signal(SignalID id);

               /**
                * Create a new signal.
                * @param id the signal
                */
            public:
               Signal(SignalID id, ProcessID);

               /**
                * Destructor
                */
            public:
               virtual ~Signal();

               /**
                * Get the signal
                * @return the signal number
                */
            public:
               inline SignalID id() const
               {
                  return _signal;
               }

               /**
                * Get the process from which this signal was sent.
                * @return the process that sent this signal
                */
            public:
               inline ProcessID processID() const
               {
                  return _process;
               }

               /** The signal */
            private:
               SignalID _signal;

               /** The process that sent the signal */
            private:
               ProcessID _process;
         };
      }
   }
}
#endif
