#include <canopy/common.h>
#include <canopy/io/FileDescriptor.h>
#include <canopy/os/ErrorStatus.h>
#include <canopy/os/ErrorStatus.h>
#include <canopy/os/SystemError.h>
#include <canopy/os/InterruptedException.h>
#include <iostream>
#include <cassert>
#include <cerrno>

#include <sys/types.h>
#include <unistd.h>
#include <poll.h>
#include <fcntl.h>

using namespace ::std;

namespace canopy {
   namespace io {
      using namespace canopy::os;

      FileDescriptor::FileDescriptor() 
            : _sd(-1), _closeDescriptor(false)
      {
      }

      FileDescriptor::FileDescriptor(Descriptor fd, bool closeWhenDone) 
            : _sd(fd), _closeDescriptor(closeWhenDone)
      {
      }

      FileDescriptor::~FileDescriptor() 
      {
         try {
            if (_closeDescriptor) {
               close();
            }
         }
         catch (const exception& e) {
            cerr << "Exception in destructor " << e.what() << endl;
         }
      }

      bool FileDescriptor::invalid() const 
      {
         return descriptor() == -1;
      }

      void FileDescriptor::close() 
      {
         Descriptor sd = descriptor();
         Descriptor invalidSd(-1);
         if (sd != -1 && _sd.compare_exchange_strong(sd, invalidSd)) {
            // close the file descriptor
            if (::close(sd) == -1) {
               // TODO: is this really what we need to do, to undo the change
               if (_sd.compare_exchange_strong(invalidSd, sd)) {
                  throw IOException(ErrorStatus::getFormattedMessage("Could not close file descriptor"));
               }
            }
            // note: we could try to free the structure, but that would mean
            // that we no longer can do equality testing on the socket
         }
      }

      bool FileDescriptor::isClosed() const 
      {
         if (descriptor() == -1) {
            return true;
         }
         // select on the socket
         ::pollfd fd = { descriptor(), POLLIN | POLLOUT | POLLPRI, 0 };
         ::std::int32_t res = ::poll(&fd, 1, 0);
         if (res == 1) {
            return (fd.revents & (POLLERR | POLLHUP | POLLNVAL)) != 0;
         }
         return res < 0;
      }

      IOEvents FileDescriptor::block(const IOEvents& events,
            const Timeout& timeout) const 
      {
         const short READ_EVENT = POLLIN;
         const short WRITE_EVENT = POLLOUT;
         const short ERROR_EVENT = POLLERR | POLLHUP | POLLNVAL;

         ::std::int64_t timeoutMS = 0;
         if (isBlockingTimeout(timeout)) {
            timeoutMS = -1;
         }
         else {
            timeoutMS = ::std::chrono::duration_cast<
                  ::std::chrono::duration< ::std::int64_t, ::std::chrono::milliseconds::period> >(timeout).count();
         }

         short pollEvents = 0;
         if (events.test(IOEvents::READ)) {
            pollEvents |= READ_EVENT;
         }
         if (events.test(IOEvents::WRITE)) {
            pollEvents |= WRITE_EVENT;
         }
         if (events.test(IOEvents::ERROR)) {
            pollEvents |= ERROR_EVENT;
         }

         ::pollfd fd = { descriptor(), pollEvents, 0 };
         int res = ::poll(&fd, 1, timeoutMS);
         IOEvents result;
         if (res > 0) {
            if (invalid()) {
               ::std::cerr << "FileDescriptor::block invalid file descriptor" << ::std::endl;
               result.addEvents(IOEvents::ERROR);
            }
            else {
               if ((fd.revents & READ_EVENT) != 0) {
                  result.addEvents(IOEvents::READ);
               }
               if ((fd.revents & WRITE_EVENT) != 0) {
                  result.addEvents(IOEvents::WRITE);
               }
               if ((fd.revents & ERROR_EVENT) != 0) {
                  result.addEvents(IOEvents::ERROR);
               }
            }
         }
         else if (res < 0) {
            if (errno == EINTR) {
               throw InterruptedException("FileDescriptor::block");
            }
            throw IOException(ErrorStatus::getFormattedMessage("FileDescriptor::block"));
         }

         return result;
      }

      ssize_t FileDescriptor::peek(char* buffer, size_t bufSize,
            const Timeout& timeout) const 
      {
         IOEvents events = block(IOEvents::READ, timeout);
         if (!events.test(IOEvents::READ)) {
            return 0;
         }

         // we cannot more bytes than we can indicate that we've read
         bufSize = min(static_cast< size_t>(0x7fffffffU), bufSize);

         ssize_t count = ::read(descriptor(), buffer, bufSize);

         if (count <= 0) {
            if (count == 0) {
               return -1; // socket closed
            }
            switch (errno) {
#if EWOULDBLOCK != EAGAIN
               case EWOULDBLOCK:
#endif
               case EAGAIN:
                  return 0; // currently no data available
               case EINTR:
                  throw InterruptedException("FileDescriptor::peek");
               default:
                  throw IOException(ErrorStatus::getFormattedMessage("FileDescriptor::peek"));
            }
         }
         return count;
      }

      ssize_t FileDescriptor::read(char* buffer, size_t bufSize,
            const Timeout& timeout) 
      {
         IOEvents events = block(IOEvents::READ, timeout);
         if (!events.test(IOEvents::READ)) {
            return 0;
         }

         // we cannot more bytes than we can indicate that we've read
         bufSize = min(static_cast< size_t>(0x7fffffffU), bufSize);
         // instead of the read system call, we use recv, because
         // we can suppress signals
         ssize_t count = ::read(descriptor(), buffer, bufSize);
         if (count <= 0) {
            if (count == 0) {
               return -1; // socket closed
            }
            switch (errno) {
#if EWOULDBLOCK != EAGAIN
               case EWOULDBLOCK:
#endif
               case EAGAIN:
                  return 0; // currently no data available
               case EINTR:
                  throw InterruptedException("FileDescriptor::read");
               default:
                  throw IOException(ErrorStatus::getFormattedMessage("FileDescriptor::read"));
            }
         }
         return count;
      }

      size_t FileDescriptor::write(const char* buffer, size_t bufSize,
            const Timeout& timeout) 
      {
         IOEvents events = block(IOEvents::WRITE, timeout);
         if (!events.test(IOEvents::WRITE)) {
            return 0;
         }

         // instead of the write system call, we use send, because
         // we can suppress signals
         ssize_t count = ::write(descriptor(), buffer, bufSize);
         if (count == -1) {
            switch (errno) {
#if EWOULDBLOCK != EAGAIN
               case EWOULDBLOCK:
#endif
               case EAGAIN:
                  return 0;
               case EINTR:
                  throw InterruptedException("FileDescriptor::write");
               case EPIPE:
               default:
                  throw IOException(ErrorStatus::getFormattedMessage("FileDescriptor::write"));
            }
         }
         return count;
      }

      void FileDescriptor::setBlockingEnabled(bool enabled) 
      {
         ::std::int32_t flags = ::fcntl(descriptor(), F_GETFL);
         if (flags != -1) {
            if (enabled && (flags & O_NONBLOCK) != 0) {
               if (::fcntl(descriptor(), F_SETFL, flags & (~O_NONBLOCK)) == 0) {
                  return;
               }
            }
            else if (!enabled && (flags & O_NONBLOCK) == 0) {
               if (::fcntl(descriptor(), F_SETFL, flags | O_NONBLOCK) == 0) {
                  return;
               }
            }
            else {
               return;
            }
         }
         throw IOException(ErrorStatus::getFormattedMessage("FileDescriptor::setBlockingEnabled"));
      }

      bool FileDescriptor::isBlockingEnabled() const 
      {
         ::std::int32_t flags = ::fcntl(descriptor(), F_GETFL);
         if (flags != -1) {
            return (flags & O_NONBLOCK) == 0;
         }
         throw IOException(ErrorStatus::getFormattedMessage("FileDescriptor::isBlockingEnabled"));
      }
   }
}
