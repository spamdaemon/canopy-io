#include <canopy/io/IOEvents.h>

namespace canopy {
   namespace io {

      constexpr int IOEvents::ALL;
   }
}

::std::ostream& operator<<(::std::ostream& stream, const ::canopy::io::IOEvents& e)
{
   stream << '{';
   const char* sep = "";
   if (e.test(::canopy::io::IOEvents::READ)) {
      stream << "READ";
      sep = "|";
   }
   if (e.test(::canopy::io::IOEvents::WRITE)) {
      stream << sep << "WRITE";
      sep = "|";
   }
   if (e.test(::canopy::io::IOEvents::ERROR)) {
      stream << sep << "ERROR";
      sep = "|";
   }
   stream << '}';
   return stream;
}
