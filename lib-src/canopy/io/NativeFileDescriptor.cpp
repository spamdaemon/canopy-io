#include <canopy/io/NativeFileDescriptor.h>
#include <canopy/io/FileDescriptor.h>

namespace canopy {
   namespace io {
      NativeFileDescriptor::~NativeFileDescriptor()

      {
      }

      ::std::shared_ptr< NativeFileDescriptor> NativeFileDescriptor::stdin()
      {
         return ::std::make_shared< FileDescriptor>(0, false);
      }
      ::std::shared_ptr< NativeFileDescriptor> NativeFileDescriptor::stdout()
      {
         return ::std::make_shared< FileDescriptor>(1, false);
      }
      ::std::shared_ptr< NativeFileDescriptor> NativeFileDescriptor::stderr()
      {
         return ::std::make_shared< FileDescriptor>(2, false);
      }

   }
}
