#ifndef _CANOPY_IO_NATIVEDESCRIPTOR_H
#define _CANOPY_IO_NATIVEDESCRIPTOR_H

#ifndef _CANOPY_IO_H
#include <canopy/io/io.h>
#endif

#ifndef _CANOPY_IO_IOEVENTS_H
#include <canopy/io/IOEvents.h>
#endif

#ifndef _CANOPY_IO_IOEXCEPTION_H
#include <canopy/io/IOException.h>
#endif

#include <atomic>
#include <memory>
#include <cstdint>

namespace canopy {
   namespace io {

      /**
       * This is class is descriptor for native object, which are identified
       * by a single integer value. On Unix system, this is typically a
       * file descriptor or a socket
       */
      class NativeDescriptor : public ::std::enable_shared_from_this< NativeDescriptor>
      {
            NativeDescriptor(const NativeDescriptor&) = delete;
            NativeDescriptor&operator=(const NativeDescriptor&) = delete;

            /** The socket descriptor */
         public:
            typedef ::std::int32_t Descriptor;

            /** An invalid descriptor */
         public:
            static const Descriptor INVALID_DESCRIPTOR = -1;

            /**
             * A default constructor
             */
         protected:
            NativeDescriptor() ;

            /**
             * Virtual destructor
             */
         public:
            virtual ~NativeDescriptor() ;

            /**
             * Get the descriptor
             * @return the descriptor or a null descriptor
             */
         public:
            virtual Descriptor descriptor() const  = 0;

            /**
             * Test if this is an invalid descriptor.
             * @return true if this socket is <em>invalid</em>
             */
         public:
            virtual bool invalid() const  = 0;

            /**
             * Block on the descriptor it is possible to use the descriptor for reading or writing.
             * @param events the event set
             * @param timeout a timeout or -1 for infinite wait
             * @return the events that are available for this   descriptor
             * @throws IOException on error
             * @throws InterruptedException if the call was interrupted
             */
         public:
            virtual IOEvents block(const IOEvents& events,
                  const Timeout& timeout) const  = 0;
      };
   }
}
#endif
