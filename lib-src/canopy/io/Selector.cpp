#include <canopy/io/Selector.h>
#include <canopy/common.h>
#include <canopy/os/InterruptedException.h>
#include <canopy/os/ErrorStatus.h>
#include <canopy/io/SelectionException.h>

#include <cerrno>
#include <cassert>
#include <mutex>
#include <iostream>
#include <algorithm>

#include <sys/poll.h>
#include <unistd.h>
#include <fcntl.h>

using namespace canopy::os;

namespace canopy {
   namespace io {

      const ::std::int32_t READ_EVENT = POLLIN;
      const ::std::int32_t WRITE_EVENT = POLLOUT;
      const ::std::int32_t ERROR_EVENT = POLLERR | POLLHUP | POLLNVAL;
      const ::std::int32_t ALL_EVENT = READ_EVENT | WRITE_EVENT | ERROR_EVENT;

      namespace {

         static const bool VERBOSE = false;

#define POLLFD (static_cast< ::pollfd*>(_data))

         static ::std::int64_t makeNativeTimeout(const Timeout& t) 
         {
            ::std::int64_t timeout;
            if (t < Timeout::zero()) {
               return timeout = -1;
            }
            else {
               auto timeoutMS = ::std::chrono::duration_cast<
                     ::std::chrono::duration< ::std::int64_t, ::std::chrono::milliseconds::period> >(t);
               timeout = timeoutMS.count();
            }
            return timeout;
         }

         /**
          * Ensure that a file descriptor is closed on exec.
          * @param fd a file descriptor
          * @return true if the operation was sucessful, false otherwise
          */
         static bool setCloseOnExec(int fd) 
         {
            int flags = ::fcntl(fd, F_GETFD);
            if (flags == -1) {
               return false;
            }
            flags |= FD_CLOEXEC;
            return ::fcntl(fd, F_SETFD, flags) == 0;
         }

         /**
          * Make the specified filedescriptor non-blocking
          * @param fd a file descriptor
          * @return true if the operation was sucessful, false otherwise
          */
         static bool setNonBlocking(int fd) 
         {
            int flags = ::fcntl(fd, F_GETFL);
            if (flags == -1) {
               return false;
            }
            flags |= O_NDELAY | O_NONBLOCK;
            return ::fcntl(fd, F_SETFL, flags) == 0;
         }

         /**
          * Read from the specified descriptor until there
          * is no more data.
          * @param fd a non-blocking filedescriptor
          */
         static void flushInputPipe(int fd) 
         {
            char buf[8];
            while (::read(fd, buf, sizeof(buf)) > 0) {
               // do nothing
            }
         }

         /**
          * Send a byte on the specified filedescriptor.
          * @param fd a non-blocking file descriptor
          */
         static void notifyPipe(int fd) 
         {
            // writing this byte will cause the
            // selector to wake up
            char buf[] = { static_cast< char>(0xfd) };
            if (::write(fd, buf, sizeof(buf)) == -1) {
               if (errno != EAGAIN) {
                  assert(false);
               }
            }
         }

         /**
          * Split the specified long into a positive int and a long
          * @param theLong a long value (in-out parameter)
          * @return the integer part of the split
          */
         static ::std::int32_t splitLong(::std::int64_t& theLong) 
         {
            ::std::int32_t theInt = static_cast< ::std::int32_t>(theLong & 0x7fffffff);
            theLong >>= 31;
            return theInt;
         }

         /**
          * Create a selection event object
          * as required the poll system call
          * @param event an event
          * @return a value compatible with ::poll
          */
         static short makePollEvents(IOEvents events) 
         {
            short pollEvents = 0;
            if (events.test(IOEvents::READ)) {
               pollEvents |= READ_EVENT;
            }
            if (events.test(IOEvents::WRITE)) {
               pollEvents |= WRITE_EVENT;
            }
            if (events.test(IOEvents::ERROR)) {
               pollEvents |= ERROR_EVENT;
            }
            return pollEvents;
         }

         /**
          * Create a selection event object
          * as required the poll system call
          * @param event an event
          * @return a value compatible with ::poll
          */
         static IOEvents makeEvents(::std::uint16_t events)
         {
            IOEvents result;
            if ((events & READ_EVENT) != 0) {
               result.addEvents(IOEvents::READ);
            }
            if ((events & WRITE_EVENT) != 0) {
               result.addEvents(IOEvents::WRITE);
            }
            if ((events & ERROR_EVENT) != 0) {
               result.addEvents(IOEvents::ERROR);
            }
            return result;
         }
      }

      Selector::Selector() 
            : _selecting(false), _modified(true), _capacity(1), _wakeupEnabled(false), _wakeupPending(false), _data(
                  new pollfd[2]), _descriptors(new Descriptor[1])
      {
         // create a pipe
         if (::pipe(_fds) == 0) {
            // make both ends of the pipe non-blocking
            if (setNonBlocking(_fds[0]) && setNonBlocking(_fds[1])) {
               if (setCloseOnExec(_fds[0]) && setCloseOnExec(_fds[1])) {
                  return;
               }
            }

            // close the descriptors
            ::close(_fds[0]);
            ::close(_fds[1]);
         }

         delete[] POLLFD;
         delete[] _descriptors;
         throw ::std::runtime_error("Failed to set up selector");
      }

      Selector::~Selector() 
      {
         clear();
         delete[] POLLFD;
         delete[] _descriptors;
         ::close(_fds[0]);
         ::close(_fds[1]);
      }

      size_t Selector::size() const 
      {
         // protect against concurrent modification
        ::std::lock_guard<::std::mutex>  guard(_mutex);
         return const_cast< EventMap&>(_events).size();
      }

      bool Selector::replace(Descriptor descriptor, IOEvents event) 
      {
         if (event.empty()) {
            return remove(descriptor, IOEvents::ALL);
         }

         // be conservative and allow non-null descriptors
         if (!descriptor) {
            return false;
         }

         // protect against concurrent modification
         ::std::lock_guard<::std::mutex>  guard(_mutex);
         EventMap& events = const_cast< EventMap&>(_events);
         EventMap::iterator i = events.find(descriptor);
         if (i != events.end()) {
            auto oldEvents = i->second;
            auto newEvents = event;
            if (newEvents == oldEvents) {
               return true;
            }
            i->second = newEvents;
         }
         else {
            return false;
         }
         _modified = true;

         // make sure to wakeup if necessary
         autoWakeup();
         return true;
      }
      void Selector::add(Descriptor descriptor, IOEvents event) 
      {
         // be conservative and allow non-null descriptors
         if (!descriptor || event.empty()) {
            return;
         }

         // protect against concurrent modification
         ::std::lock_guard<::std::mutex>  guard(_mutex);
         EventMap& events = const_cast< EventMap&>(_events);
         EventMap::iterator i = events.find(descriptor);
         if (i != events.end()) {
            auto oldEvents = i->second;
            auto newEvents = oldEvents | event;
            if (newEvents == oldEvents) {
               return;
            }
            i->second = newEvents;
         }
         else {
            events.insert(EventMap::value_type(descriptor, event));
         }
         _modified = true;

         // make sure to wakeup if necessary
         autoWakeup();
      }

      bool Selector::remove(Descriptor descriptor, IOEvents event) 
      {
         if (!descriptor) {
            return true;
         }

         // protect against concurrent execution
         ::std::lock_guard<::std::mutex>  guard(_mutex);
         EventMap& events = const_cast< EventMap&>(_events);
         EventMap::iterator i = events.find(descriptor);
         if (i != events.end()) {
            auto oldEvents = i->second;
            auto newEvents = oldEvents & ~event;
            if (newEvents == oldEvents) {
               return false;
            }
            _modified = true;

            // make sure to wakeup if necessary
            autoWakeup();

            if (newEvents == 0) {
               events.erase(i);
               // descriptor's been removed completely
               return true;
            }
            i->second = newEvents;
            return false;
         }
         return true;
      }

      void Selector::wakeup() 
      {
         ::std::lock_guard<::std::mutex>  guard(_mutex);
         if (!_wakeupPending) {
            notifyPipe(_fds[1]);
            _wakeupPending = true;
         }
      }

      void Selector::autoWakeup() volatile 
      {
         if (_selecting && _wakeupEnabled) {
            notifyPipe(_fds[1]);
            _wakeupPending = true;
         }
      }

      bool Selector::clearPendingWakeup() 
      {
         ::std::lock_guard<::std::mutex>  guard(_mutex);
         bool pending = _wakeupPending;
         _wakeupPending = false;
         return pending;
      }

      void Selector::clear() 
      {
         ::std::lock_guard<::std::mutex>  guard(_mutex);
         EventMap& events = const_cast< EventMap&>(_events);
         if (events.size() > 0) {
            events.clear();
            const_cast< EventMap&>(_events).clear();
            _modified = true;

            // wakeup if currently selection and
            // we should auto interrupt the selection
            autoWakeup();
         }
      }

      bool Selector::isSelectable(Descriptor descriptor) const 
      {
         ::std::lock_guard<::std::mutex>  guard(_mutex);
         EventMap& events = const_cast< EventMap&>(_events);
         return events.find(descriptor) != events.end();
      }
      Selector::Events Selector::select(const Timeout& timeoutNS,
            Events&& result) 

      {
         result.clear();
         EventMap emap = select(timeoutNS,EventMap());
         result.reserve(emap.size());
         for (auto& pair : emap) {
            result.push_back(DescriptorEvent(pair.first,pair.second));
         }
         return ::std::move(result);
      }


      Selector::EventMap Selector::select(const Timeout& timeoutNS,
            EventMap&& result) 
      {
         ::std::int64_t timeout = makeNativeTimeout(timeoutNS);

         ::std::lock_guard<::std::mutex>  guard(_mutex);

         if (_selecting) {
            throw SelectionException(SelectionException::CONCURRENT_MODIFICATION);
         }

         EventMap& events = const_cast< EventMap&>(_events);

         // clear the last selection set
         result.clear();

         _selecting = true;

         while (!_wakeupPending && result.size() == 0) {
            size_t count = events.size();

            // if the descriptor set was modified, then refresh here
            if (_modified) {

               // resize if necessary (capacity is at least 1)
               if (_capacity < count) {
                  delete[] POLLFD;
                  delete[] _descriptors;
                  _capacity = 2 * count;
                  _data = new ::pollfd[_capacity + 1];
                  _descriptors = new Descriptor[_capacity];
               }

               // copy new values into the POLLFD structure
               ::std::uint32_t j = 0;
               for (EventMap::iterator i = events.begin(); i != events.end();) {
                  _descriptors[j] = i->first;
                  POLLFD[j].fd = _descriptors[j]->descriptor();
                  if (POLLFD[j].fd == -1) {
                     // remove this, because we can't get an event on this descriptor.
                     result.insert(EventMap::value_type(_descriptors[j], IOEvents::ERROR));
                     events.erase(i++);
                  }
                  else {
                     POLLFD[j].events = makePollEvents(i->second);
                     POLLFD[j].revents = 0;
                     ++j;
                     ++i;
                  }
               }
               count = j;

               // setup the control fd
               POLLFD[count].fd = _fds[0];
               POLLFD[count].events = POLLIN;
               POLLFD[count].revents = 0;

               while (j < _capacity) {
                  _descriptors[j++] = Descriptor();
               }
            }
            _modified = false;

            // flush the input queue
            flushInputPipe(_fds[0]);

            // unlock the mutex while selection is in progress
            _mutex.unlock();

            // the default is timeout
            ::std::int32_t res = 0;

            if (timeout < 0) {
               res = ::poll(POLLFD, count + 1, -1);
            }
            else {
               ::std::int32_t tm = splitLong(timeout);
               timeout -= tm;
               // poll for some events
               res = ::poll(POLLFD, count + 1, tm);
            }
            _mutex.lock();

            // if an error occurred throw an exception
            if (res == -1) {
               // no longer selecting
               _selecting = false;

               if (errno == EINTR) {
                  throw InterruptedException("Selector::select");
               }
               throw SelectionException(ErrorStatus::getFormattedMessage("Select failed"));
            }

            // copy the result set and remove those descriptors which are of no use
            for (::std::uint32_t i = 0; i < count; ++i) {
               Descriptor descriptor = _descriptors[i];
               EventMap::iterator j = events.find(descriptor);
               if (j != events.end() && POLLFD[i].revents != 0) {
                  if ((POLLFD[i].revents & POLLFD[i].events) != 0) {
                     result.insert(EventMap::value_type(descriptor, makeEvents(POLLFD[i].revents)));
                  }
                  // if an error occurred on the specified descriptor, remove it
                  else if (descriptor->descriptor() == -1 || (POLLFD[i].revents & (ERROR_EVENT)) != 0) {
                     events.erase(j);
#if 0
                     ::std::cerr << "System descriptor error: descriptor=" << descriptor.descriptor() << ", code = "
                           << POLLFD[i].revents << ::std::endl;
#endif
                     result.insert(EventMap::value_type(descriptor, IOEvents::ERROR));
                     _modified = true;
                  }
                  else {
#if 0
                     ::std::cerr << "Selector got events we're not selecting for: " << descriptor.descriptor()
                           << ", pollfd=" << POLLFD[i].revents << " vs. " << POLLFD[i].events << ::std::endl;
#endif
                  }
               }
            }

            // check if we received a signal on the control filedescriptor
            if ((POLLFD[count].revents & POLLIN) == POLLIN) {
               break;
            }
            // check if we received a signal on the control filedescriptor
            if (timeout <= 0) {
               break;
            }
         }
         // clear pending wakeups
         _wakeupPending = false;

         // no longer selecting
         _selecting = false;

         // return the result directly, because otherwise a dead-lock will ensue
         return ::std::move(result);
      }

      IOEvents Selector::select(Descriptor descriptor, IOEvents event,
            const Timeout& timeoutNS) 
      {
         // these methods cannot deal with a timeout in nanoseconds
         ::std::int64_t timeout = makeNativeTimeout(timeoutNS);

         auto events = makePollEvents(event);
         if (events == 0) {
            return 0;
         }

         // the default is timeout
         ::std::int32_t res = 0;

         // the timeout may be larger than the allowed
         // timeout for poll(). We'll poll in a loop; this
         // is obviously not practical
         ::pollfd fd = { descriptor->descriptor(), events, 0 };

         do {
            // poll for some events
            if (timeout < 0) {
               res = ::poll(&fd, 1, -1);
            }
            else {
               ::std::int32_t tm = splitLong(timeout);
               timeout -= tm;
               res = ::poll(&fd, 1, tm);
            }

            if (res != 0) {
               if (res == -1) {
                  if (errno == EINTR) {
                     throw InterruptedException("Selector::select");
                  }
                  throw SelectionException(ErrorStatus::getFormattedMessage("Select failed"));
               }
               if (descriptor->descriptor() == -1 || (fd.revents & (ERROR_EVENT)) != 0) {
                  ::std::cerr << "System descriptor error: code = " << fd.revents << ::std::endl;
                  return IOEvents::ERROR;
               }
               return makeEvents(fd.revents);
            }
         } while (res == 0 && timeout > 0);
         return 0;
      }

      IOEvents Selector::getRegisteredEvents(Descriptor descriptor) const 
      {
         // protect this section from concurrent execution
         ::std::lock_guard<::std::mutex>  guard(_mutex);
         const EventMap& events = const_cast< const EventMap&>(_events);
         EventMap::const_iterator i = events.find(descriptor);
         if (i != events.end()) {
            // cannot be 0, because that would mean the descriptor is not registered.
            assert(!i->second.empty());
            return i->second;
         }
         return 0;
      }

   }
}
