#ifndef _CANOPY_IO_NET_NETWORK_H
#define _CANOPY_IO_NET_NETWORK_H

#ifndef _CANOPY_IO_NET_ADDRESS_H
#include <canopy/io/net/Address.h>
#endif

#ifndef _CANOPY_IO_NET_H
#include <canopy/io/net/net.h>
#endif

#ifndef _CANOPY_IO_NET_NETEXCEPTION_H
#include <canopy/io/net/NetException.h>
#endif

#ifndef _CANOPY_IO_NET_ENDPOINT_H
#include <canopy/io/net/Endpoint.h>
#endif

#include <memory>
#include <vector>
#include <functional>
#include <string>
#include <iosfwd>
#include <cstdint>
#include <cstddef>
#include <ostream>

namespace canopy {
   namespace io {
      namespace net {
         class Address;

         /**
          * A network represents a given network attached to the host computer.
          *
          */
         class Network
         {
               /** The network prefix */
            public:
               typedef ::std::uint16_t Prefix;

               /**
                *  Network flags.
                */
            public:
               enum Flags
               {
                  UP = 1, RUNNING = 2, MULTICAST = 4, BROADCAST = 8, POINT_TO_POINT = 16, LOOPBACK = 32
               };

               /** A private default constructor */
            private:
               Network();

               /**
                * A network object
                * @param name the name of a network
                */
            public:
               Network(const ::std::string& name);

               /**
                * Copy a network object.
                * @param src an object
                */
            public:
               Network(const Network& src);

               /**
                * Move a network object.
                * @param src an object
                */
            public:
               Network(Network&& src);

               /**
                * Destroys this Network object.
                */
            public:
               ~Network();

               /**
                * A copy operator.
                * @param src a src object
                * @return *this
                */
            public:
               Network& operator=(Network src);

               /**
                * Enumerate all currently known network interfaces.
                * @param cb a callback
                * @param domain the domain the interface should have or 0 for any
                * @param flags the flags that the interface must support
                * @return number of interfaces enumerated
                */
            public:
               static size_t enumerateInterfaces(::std::function< void(const Network&)> cb, Domain domain = Domain(),
                     ::std::uint32_t flags = 0);

               /**
                * Get the list of networks.
                * @param domain the domain the interface should have or 0 for any
                * @param flags the flags that the interface must support
                * @return an array of networks in unspecified order
                */
            public:
               static ::std::vector< Network> getNetworks(Domain domain = Domain(), ::std::uint32_t flags = 0);

               /**
                * Get the names of the known networks.
                * @param domain the domain the interface should have or 0 for any
                * @param flags the flags that the interface must support
                * @return the names of known networks in unspecified order
                */
            public:
               static ::std::vector< ::std::string> getNetworkNames(Domain domain = Domain(),
                     ::std::uint32_t flags = 0);

               /**
                * Get the network with the specified name and domain. If the domain is 0,
                * the result is an arbitrary network with the same name.
                * @param name the name of a network
                * @param domain the domain of the network
                * @return a network
                * @throws NetException if there is no such network
                */
            public:
               static Network getNetworkByName(const ::std::string& name, Domain domain);

               /**
                * Get the name of the interface to which this network is  attached. Multiple
                * networks may share the same name.
                * @return the name of the interface.
                */
            public:
               const ::std::string& interface() const
               {
                  return _interface;
               }

               /**
                * Get the flags.
                * @return the flags for this network.
                */
            public:
               inline ::std::uint32_t flags() const
               {
                  return _flags;
               }

               /**
                * Test if the specified flags are set.
                * @param f a combination of flags to test
                * @return true if any of the specified flags are set
                */
            public:
               inline bool testFlags(::std::uint32_t f) const
               {
                  return (_flags & f) != 0;
               }

               /**
                * Get the network prefix (if defined).
                * @return the network prefix or 0 if not defined
                */
            public:
               inline Prefix getPrefix() const
               {
                  return _networkPrefix;
               }

               /**
                * Get the network's address as an endpoint.
                * @param t the socket type
                * @param p the protocol
                * @return the broadcast address or nullptr if not possible to create
                */
            public:
               inline const Endpoint& endpoint() const
               {
                  return _address;
               }

               /**
                * Get the domain of this network.
                * @return the domain associated with the address of endpoint().
                */
            public:
               inline Domain domain() const
               {
                  return _address.address().domain();
               }

               /**
                * Get a broadcast address for the specified socket type and protcol.
                * @param t the socket type
                * @param p the protocol
                * @return the broadcast address or nullptr if not possible to create
                */
            public:
               inline const Endpoint& getBroadcastAddress() const
               {
                  return _broadcast;
               }

               /**
                * Get the point-to-point address for the specified socket type and protcol.
                * @param t the socket type
                * @param p the protocol
                * @return the broadcast address or nullptr if not possible to create
                */
            public:
               inline const Endpoint& getPointToPointAddress() const
               {
                  return _point2point;
               }

               /**
                * Convert a  sockaddr to a prefix
                * @param addr a sock addr
                * @return a prefix
                */
            private:
               static Prefix netmask2prefix(const ::sockaddr& addr);

               /** The name of the network */
            private:
               ::std::string _interface;

               /** The flags */
            private:
               ::std::uint32_t _flags;

               /** The netmask as a prefix */
            private:
               Prefix _networkPrefix;

               /** The network address */
            private:
               Endpoint _address;

               /** The broadcast address */
            private:
               Endpoint _broadcast;

               /** The point2point address */
            private:
               Endpoint _point2point;
         };
      }
   }
}
::std::ostream& operator<<(::std::ostream& out, const ::canopy::io::net::Network& net);

#endif

