#include <canopy/io/net/AddressException.h>
#include <memory>

namespace canopy {
   namespace io {
      namespace net {

         AddressException::AddressException(::std::string s)
               : NetException(::std::move(s))
         {
         }

      }
   }
}
