#include <timber/logging.h>
#include <canopy/common.h>
#include <canopy/io/FileDescriptor.h>
#include <canopy/io/io.h>
#include <canopy/io/IOEvents.h>
#include <canopy/io/net/Address.h>
#include <canopy/io/net/ConnectionRefused.h>
#include <canopy/io/net/net.h>
#include <canopy/io/net/PosixSocket.h>
#include <canopy/io/net/SocketException.h>
#include <canopy/os/ErrorStatus.h>
#include <canopy/os/SystemError.h>
#include <canopy/os/InterruptedException.h>

#include <algorithm>
#include <cassert>
#include <cerrno>
#include <iostream>

#include <fcntl.h>
#include <poll.h>
#include <stddef.h>
#include <sys/socket.h>
#include <unistd.h>

using namespace ::std;

namespace canopy {
   namespace io {
      namespace net {
         using namespace ::canopy::os;
         using namespace ::timber::logging;
         namespace {
            static ::timber::logging::Log log()
            {
               return ::timber::logging::Log("canopy.io.net.PosixSocket");
            }

            /**
             * Ensure that a file descriptor is closed on exec.
             * @param fd a file descriptor
             * @return true if the operation was sucessful, false otherwise
             */
            static bool setCloseOnExec(int fd)
            {
               int flags = ::fcntl(fd, F_GETFD);
               if (flags == -1) {
                  return false;
               }
               flags |= FD_CLOEXEC;
               return ::fcntl(fd, F_SETFD, flags) == 0;
            }

         }

         PosixSocket::PosixSocket()
               : _sd(-1), _domain(UNIX), _type(STREAM)
         {
         }

         PosixSocket::PosixSocket(const SocketType& t, const Domain& d, const Protocol& p, SocketDescriptor sd)
               : _sd(sd), _domain(d), _type(t), _protocol(p)
         {
            if (!setCloseOnExec(sd)) {
               ::close(sd);
               throw SocketException(ErrorStatus::getFormattedMessage("Could not set socket flag FD_CLOEXEC"));
            }
         }

         PosixSocket::PosixSocket(const SocketType& t, const Domain& d, const Protocol& p)
               : _domain(d), _type(t), _protocol(p)
         {
            int sd = ::socket(d.id(), t.id(), p.id());
            if (sd < 0) {
               ::timber::logging::LogEntry e(log());
               e.warn().warn() << "Could not create socket: PosixSocket(" << t.id() << ", " << d.id() << ", " << p.id()
                     << ")" << doLog;
               throw SocketException(ErrorStatus::getFormattedMessage("Could not create socket"));
            }
            else {
#if 0
               LogEntry(log()).warn() << "Created PosixSocket(" << t.id() << ", " << d.id() << ", " << p.id() << ")" << doLog;
#endif
            }
            if (!setCloseOnExec(sd)) {
               ::close(sd);
               throw SocketException(ErrorStatus::getFormattedMessage("Could not set socket flag FD_CLOEXEC"));
            }

            _sd = sd;

         }

         PosixSocket::~PosixSocket()
         {
            try {
               close();
            }
            catch (const exception& e) {
               cerr << "Exception in Socket destructor " << e.what() << endl;
            }
         }

         ::canopy::io::IOEvents PosixSocket::block(const ::canopy::io::IOEvents& events, const Timeout& timeout) const
         {
            ::canopy::io::FileDescriptor fd(descriptor(), false);
            return fd.block(events, timeout);
         }

         void PosixSocket::close()
         {
            SocketDescriptor sd = descriptor();
            SocketDescriptor invalidSd(-1);
            if (sd != -1 && _sd.compare_exchange_strong(sd, invalidSd)) {
               // close the file descriptor
               if (::close(sd) == -1) {
                  // TODO: is this really what we need to do, to undo the change
                  if (_sd.compare_exchange_strong(invalidSd, sd)) {
                     throw SocketException(ErrorStatus::getFormattedMessage("Could not close socket"));
                  }
               }
               // note: we could try to free the structure, but that would mean
               // that we no longer can do equality testing on the socket
            }
         }

         bool PosixSocket::isClosed() const
         {
            if (descriptor() == -1) {
               return true;
            }
            // select on the socket
            ::pollfd fd = { descriptor(), POLLIN | POLLOUT | POLLPRI, 0 };
            ::std::int32_t res = ::poll(&fd, 1, 0);
            if (res == 1) {
               return (fd.revents & (POLLERR | POLLHUP | POLLNVAL)) != 0;
            }
            return res < 0;
         }

         void PosixSocket::shutdownWrite()
         {
            if (::shutdown(descriptor(), SHUT_WR) == -1) {
               throw SocketException(ErrorStatus::getFormattedMessage("Could not shutdown for write"));
            }
         }

         void PosixSocket::shutdownRead()
         {
            if (::shutdown(descriptor(), SHUT_RD) == -1) {
               throw SocketException(ErrorStatus::getFormattedMessage("Could not shutdown for read"));
            }
         }

         size_t PosixSocket::writeTo(const char* buffer, size_t bufSize, const Address& to, const Timeout& timeout)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::WRITE, timeout);
               if (!events.test(::canopy::io::IOEvents::WRITE)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            ssize_t count = ::sendto(descriptor(), buffer, bufSize, flags, to.sockaddr(), to._addrlen);

            if (count == -1) {
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0;
                  case EINTR:
                     throw InterruptedException("PosixSocket::writeTo");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  case ECONNREFUSED:
                     // this one may be thrown if the socket is a udp socket
                     throw ConnectionRefused();
                  default:
                     throw SocketException(ErrorStatus::getFormattedMessage("writeTo"));
               }
            }
            return count;
         }

         ssize_t PosixSocket::readFrom(char* buffer, size_t bufSize, Address& from, const Timeout& timeout)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            // we cannot more bytes than we can indicate that we've read
            bufSize = min(static_cast< size_t>(0x7fffffff), bufSize);

            ::socklen_t len = sizeof(from._address);
            ssize_t count = ::recvfrom(descriptor(), buffer, bufSize, flags, from.sockaddr(), &len);

            if (count <= 0) {
               if (count == 0) {
                  return -1; // socket closed
               }
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0; // no data right now (non-blocking)
                  case EINTR:
                     throw InterruptedException("PosixSocket::readFrom");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  default:
                     throw SocketException(ErrorStatus::getFormattedMessage("readFrom"));
               }
            }
            from._addrlen = len;
            from._domain = _domain;
            from._type = _type;
            from._protocol = _protocol;
            return count;
         }

         ssize_t PosixSocket::peek(char* buffer, size_t bufSize, const Timeout& timeout) const
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            flags |= MSG_PEEK; // we peek (this is posix)

            // we cannot more bytes than we can indicate that we've read
            bufSize = min(static_cast< size_t>(0x7fffffffU), bufSize);
            // instead of the read system call, we use recv, because
            // we can suppress signals
            ssize_t count = ::recv(descriptor(), buffer, bufSize, flags);
            if (count <= 0) {
               if (count == 0) {
                  return -1; // socket closed
               }
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0; // currently no data available
                  case EINTR:
                     throw InterruptedException("PosixSocket::read");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  default:
                     throw SocketException(SocketException::OTHER);
               }
            }
            return count;
         }

         ssize_t PosixSocket::read(char* buffer, size_t bufSize, const Timeout& timeout)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::READ, timeout);
               if (!events.test(::canopy::io::IOEvents::READ)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            // we cannot more bytes than we can indicate that we've read
            bufSize = min(static_cast< size_t>(0x7fffffffU), bufSize);
            // instead of the read system call, we use recv, because
            // we can suppress signals
            ssize_t count = ::recv(descriptor(), buffer, bufSize, flags);
            if (count <= 0) {
               if (count == 0) {
                  return -1; // socket closed
               }
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0; // currently no data available
                  case EINTR:
                     throw InterruptedException("PosixSocket::read");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  default:
                     throw SocketException(SocketException::OTHER);
               }
            }
            return count;
         }

         size_t PosixSocket::write(const char* buffer, size_t bufSize, const Timeout& timeout)
         {
            if (::canopy::io::isFiniteTimeout(timeout)) {
               auto events = block(::canopy::io::IOEvents::WRITE, timeout);
               if (!events.test(::canopy::io::IOEvents::WRITE)) {
                  return 0;
               }
            }

            int flags(0);
#if CANOPY_OS != CANOPY_SUNOS
            flags = MSG_NOSIGNAL;
#endif
            if (::canopy::io::isNonBlockingTimeout(timeout)) {
               flags |= MSG_DONTWAIT;
            }
            // instead of the write system call, we use send, because
            // we can suppress signals
            ssize_t count = ::send(descriptor(), buffer, bufSize, flags);
            if (count == -1) {
               switch (errno) {
#if EWOULDBLOCK != EAGAIN
                  case EWOULDBLOCK:
#endif
                  case EAGAIN:
                     return 0;
                  case EINTR:
                     throw InterruptedException("PosixSocket::write");
                  case EPIPE:
                     throw SocketException(SocketException::CONNECTION_BROKEN);
                  case ECONNREFUSED:
                     // this one may be thrown if the socket is a udp socket
                     throw ConnectionRefused();
                  default:
                     throw SocketException(SocketException::OTHER);
               }
            }
            return count;
         }

         SocketType PosixSocket::type() const
         {
            return _type;
         }

         Domain PosixSocket::domain() const
         {
            return _domain;
         }

         Protocol PosixSocket::protocol() const
         {
            return _protocol;
         }

         void PosixSocket::setBlockingEnabled(bool enabled)
         {
            ::std::int32_t flags = ::fcntl(descriptor(), F_GETFL);
            if (flags != -1) {
               if (enabled && (flags & O_NONBLOCK) != 0) {
                  if (::fcntl(descriptor(), F_SETFL, flags & (~O_NONBLOCK)) == 0) {
                     return;
                  }
               }
               else if (!enabled && (flags & O_NONBLOCK) == 0) {
                  if (::fcntl(descriptor(), F_SETFL, flags | O_NONBLOCK) == 0) {
                     return;
                  }
               }
               else {
                  return;
               }
            }
            throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: setBlockingEnabled"));
         }

         bool PosixSocket::isBlockingEnabled() const
         {
            ::std::int32_t flags = ::fcntl(descriptor(), F_GETFL);
            if (flags != -1) {
               return (flags & O_NONBLOCK) == 0;
            }
            throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: isBlockingEnabled"));
         }

         void PosixSocket::setSendBufferSize(::std::uint32_t size)
         {
            ::std::int32_t res = ::setsockopt(descriptor(), SOL_SOCKET, SO_SNDBUF, &size, sizeof(size));
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: setSendBufferSize"));
            }
         }

         ::std::uint32_t PosixSocket::getSendBufferSize() const
         {
            ::std::uint32_t size;
            ::socklen_t len = sizeof(size);
            ::std::int32_t res = ::getsockopt(descriptor(), SOL_SOCKET, SO_SNDBUF, &size, &len);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: getSendBufferSize"));
            }
            return size;
         }

         void PosixSocket::setReceiveBufferSize(::std::uint32_t size)
         {
            ::std::int32_t res = ::setsockopt(descriptor(), SOL_SOCKET, SO_RCVBUF, &size, sizeof(size));
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: setReceiveBufferSize"));
            }
         }

         ::std::uint32_t PosixSocket::getReceiveBufferSize() const
         {
            ::std::uint32_t size;
            ::socklen_t len = sizeof(size);
            len = sizeof(size);
            ::std::int32_t res = ::getsockopt(descriptor(), SOL_SOCKET, SO_RCVBUF, &size, &len);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: getReceiveBufferSize"));
            }
            return size;
         }

         void PosixSocket::clearErrors()
         {
            ::std::int32_t error;
            ::socklen_t sz = sizeof(error);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_ERROR, &error, &sz);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: clearErrors"));
            }
         }

         bool PosixSocket::connect(const Address& addr)
         {

            int res = ::connect(descriptor(), addr.sockaddr(), addr._addrlen);
            if (res != 0) {
               switch (errno) {
                  case EISCONN:
                     throw SocketException(SocketException::ALREADY_CONNECTED);
                  case ECONNREFUSED:
                     throw ConnectionRefused();
                  case ECONNRESET:
                     throw SocketException(SocketException::CONNECTION_RESET);
                  case ETIMEDOUT:
                     throw SocketException(SocketException::CONNECTION_TIMED_OUT);
                  case ENETUNREACH:
                     throw SocketException(SocketException::NETWORK_UNREACHABLE);
                  case EINPROGRESS:
                     return false;
                  case EALREADY:
                     throw SocketException(SocketException::ALREADY_CONNECTED);
                  case EAFNOSUPPORT:
                     throw SocketException(SocketException::INVALID_ADDRESS);
                  case EACCES: // fall through
                  case EPERM:
                     throw SocketException(SocketException::DENIED);
                  default:
                     throw SocketException(ErrorStatus::getFormattedMessage("Could not connect"));
               }
            }
            return true;
         }

         void PosixSocket::bind(const Address& addr)
         {
            int res = ::bind(descriptor(), addr.sockaddr(), addr._addrlen);

            // in an error occurred, then throw an exception
            if (res != 0) {
               switch (errno) {
                  case EINVAL:
                     throw SocketException(SocketException::ALREADY_BOUND);
                  case EACCES:
                     throw SocketException(SocketException::DENIED);
                  case EADDRINUSE:
                     throw SocketException(SocketException::ALREADY_USED);
                  default:
                     throw SocketException(ErrorStatus::getFormattedMessage("Could not bind"));
               }
            }
         }

         bool PosixSocket::isServerSocket() const
         {
            // set the reuse address option
            int enabled = 0;
            ::socklen_t len = sizeof(enabled);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_ACCEPTCONN, &enabled, &len);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_ACCEPTCONN"));
            }
            return enabled != 0;
         }

         void PosixSocket::setReuseAddressEnabled(bool enabled)
         {
            // set the reuse address option
            int enable = (int) enabled;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable));
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_REUSEADDR"));
            }
         }

         bool PosixSocket::isReuseAddressEnabled() const
         {
            // set the reuse address option
            int enabled = 0;
            ::socklen_t len = sizeof(enabled);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_REUSEADDR, &enabled, &len);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_REUSEADDR"));
            }
            return enabled != 0;
         }

         void PosixSocket::setBroadcastEnabled(bool enabled)
         {
            // set the reuse address option
            int enable = (int) enabled;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_BROADCAST, &enable, sizeof(enable));
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_BROADCAST"));
            }
         }

         bool PosixSocket::isBroadcastEnabled() const
         {
            // set the reuse address option
            int enabled = 0;
            ::socklen_t len = sizeof(enabled);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_BROADCAST, &enabled, &len);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_BROADCAST"));
            }
            return enabled != 0;
         }

         void PosixSocket::setKeepAliveEnabled(bool enabled)
         {
            int enable = (int) enabled;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_KEEPALIVE, &enable, sizeof(enable));
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_KEEPALIVE"));
            }
         }

         bool PosixSocket::isKeepAliveEnabled() const
         {
            int enabled = 0;
            ::socklen_t len = sizeof(enabled);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_KEEPALIVE, &enabled, &len);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_KEEPALIVE"));
            }
            return enabled != 0;
         }

         void PosixSocket::enableLinger(::std::uint32_t duration)
         {
            ::linger data;
            data.l_onoff = 1;
            data.l_linger = duration;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_LINGER, &data, sizeof(data));
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_LINGER"));
            }
         }

         void PosixSocket::disableLinger()
         {
            ::linger data;
            data.l_onoff = 0;
            data.l_linger = 0;
            int res = ::setsockopt(descriptor(), SOL_SOCKET, SO_LINGER, &data, sizeof(data));
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_LINGER"));
            }
         }

         ::std::uint32_t PosixSocket::getLingerTime() const
         {
            ::socklen_t sz;
            ::linger data;
            data.l_onoff = 0;
            data.l_linger = 0;
            sz = sizeof(data);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_LINGER, &data, &sz);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_LINGER"));
            }
            return data.l_linger;
         }

         bool PosixSocket::isLingerEnabled() const
         {
            ::socklen_t sz;
            ::linger data;
            data.l_onoff = 0;
            data.l_linger = 0;
            sz = sizeof(data);
            int res = ::getsockopt(descriptor(), SOL_SOCKET, SO_LINGER, &data, &sz);
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Socket option error: SO_LINGER"));
            }
            return data.l_onoff != 0;
         }

         Address PosixSocket::getBoundAddress() const
         {
            // got the peer name
            Address addr;
            ::socklen_t len = sizeof(addr._address);
            int res = ::getsockname(descriptor(), addr.sockaddr(), &len);
            // if an error occured, throw an IO exception
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("getBoundAddress failed"));
            }
            assert(len <= sizeof(addr._address));
            addr._domain = _domain;
            addr._type = _type;
            addr._protocol = _protocol;
            addr._addrlen = len;
            return addr;
         }

         Address PosixSocket::getPeerAddress() const
         {
            Address addr;
            ::socklen_t len = sizeof(addr._address);

            // got the peer name
            int res = ::getpeername(descriptor(), addr.sockaddr(), &len);
            // if an error occured, throw an IO exception
            if (res != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("getPeerAddress failed"));
            }
            assert(len <= sizeof(addr._address));
            addr._domain = _domain;
            addr._type = _type;
            addr._protocol = _protocol;
            addr._addrlen = len;
            return addr;
         }

         void PosixSocket::listen(::std::uint32_t backlog)
         {
            if (::listen(descriptor(), backlog) != 0) {
               throw SocketException(ErrorStatus::getFormattedMessage("Cannot listen on socket"));
            }
         }

         ::std::unique_ptr< Socket> PosixSocket::accept(Address* addr)
         {
            // accept a connection
            socklen_t len = sizeof(sockaddr_storage);
            int sd = ::accept(descriptor(), addr?addr->sockaddr():nullptr, &len);

            // if there is no error, then return
            // the socket
            if (sd != -1) {
               if (addr) {
                  addr->_domain = _domain;
                  addr->_type = _type;
                  addr->_protocol = _protocol;
                  addr->_addrlen = len;
               }
               // the address has already been set
               return ::std::unique_ptr< Socket>(new PosixSocket(_type, _domain, _protocol, sd));
            }
            // this may create warning, if EAGAIN and EWOULDBLOCK are the same
            if ((errno == EAGAIN)
#if EAGAIN != EWOULDBLOCK
            || (errno == EWOULDBLOCK)
#endif
            ) {
               //FIXME: what to do; do'nt like this one at all
               //but throwing ResourceUnvailable is not too good either
               return ::std::unique_ptr< Socket>();
            }

            switch (errno) {
               case ECONNABORTED:
                  throw SocketException(SocketException::CONNECTION_ABORTED);
               case EOPNOTSUPP:
                  throw SocketException(SocketException::UNSUPPORTED_OPERATION);
               case EPERM:
                  throw SocketException(SocketException::DENIED);
               case ENOBUFS: // fall through
               case ENOMEM:
                  throw SocketException(SocketException::NO_RESOURCES);
               default:
                  throw SocketException(ErrorStatus::getFormattedMessage("Could not accept connection"));
            }
         }

      }
   }
}
