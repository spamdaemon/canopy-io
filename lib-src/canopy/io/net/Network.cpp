#include <canopy/io/net/Network.h>
#include <canopy/os/SystemError.h>
#include <canopy/common.h>
#include <timber/logging.h>

#include <functional>
#include <iostream>
#include <set>
#include <cstdint>
#include <cstddef>

#include <ifaddrs.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/socket.h>
#include <linux/un.h>
#include <netpacket/packet.h>

using namespace ::std;
using namespace ::canopy::os;

namespace canopy {
   namespace io {
      namespace net {
         using namespace ::timber::logging;
         namespace {

            inline ::timber::logging::Log log()
            {
               return ::timber::logging::Log("canopy.io.net.Network");
            }

            static uint32_t toNetworkFlags(uint32_t flags)
            {
               uint32_t res = 0;
               if (flags & IFF_MULTICAST) {
                  res |= Network::MULTICAST;
               }
               if (flags & IFF_BROADCAST) {
                  res |= Network::BROADCAST;
               }
               if (flags & IFF_UP) {
                  res |= Network::UP;
               }
               if (flags & IFF_RUNNING) {
                  res |= Network::RUNNING;
               }
               if (flags & IFF_LOOPBACK) {
                  res |= Network::LOOPBACK;
               }
               return res;
            }

            static size_t getAddressLength(int domain)
            {
               switch (domain) {
                  case PF_INET:
                     return sizeof(::sockaddr_in);
                  case PF_INET6:
                     return sizeof(::sockaddr_in6);
                  case PF_LOCAL:
                     return sizeof(::sockaddr_un);
                  case PF_PACKET:
                     return sizeof(::sockaddr_ll);
                  default:
                     return 0;
               }
            }

         }
         Network::Network()
               : _flags(0), _networkPrefix(0)
         {
         }

         Network::~Network()
         {
         }

         Network::Network(const ::std::string& nname)
               : _interface(nname), _flags(0), _networkPrefix(0)
         {
         }

         Network::Network(const Network& src)
               : _interface(src._interface), _flags(src._flags), _networkPrefix(src._networkPrefix), _address(
                     src._address), _broadcast(src._broadcast), _point2point(src._point2point)
         {
         }

         Network::Network(Network&& src)
               : _interface(::std::move(src._interface)), _flags(src._flags), _networkPrefix(
                     ::std::move(src._networkPrefix)), _address(::std::move(src._address)), _broadcast(
                     ::std::move(src._broadcast)), _point2point(::std::move(src._point2point))
         {
         }

         Network& Network::operator=(Network src)
         {
            _interface = ::std::move(src._interface);
            _flags = src._flags;
            _networkPrefix = ::std::move(src._networkPrefix);
            _address = ::std::move(src._address);
            _broadcast = ::std::move(src._broadcast);
            _point2point = ::std::move(src._point2point);
            return *this;
         }

         Network Network::getNetworkByName(const ::std::string& nwName, Domain domain)
         {
            Network res;
            enumerateInterfaces([&res,&nwName] (const Network& nw) {if (nw.interface()==nwName) {res = nw;}}, domain,
                  0);
            if (res._address.empty()) {
               throw NetException("No such network " + nwName);
            }
            return res;
         }

         ::std::vector< ::std::string> Network::getNetworkNames(Domain domain, ::std::uint32_t requiredFlags)
         {
            ::std::set< ::std::string> res;

            enumerateInterfaces([&res] (const Network& nw) {res.insert(nw.interface());}, domain, requiredFlags);

            return ::std::vector< ::std::string>(res.begin(), res.end());

         }

         ::std::vector< Network> Network::getNetworks(Domain domain, ::std::uint32_t requiredFlags)
         {
            ::std::vector< Network> res;
            enumerateInterfaces([&res] (const Network& nw) {res.push_back(nw);}, domain, requiredFlags);
            return res;
         }

         size_t Network::enumerateInterfaces(::std::function< void(const Network&)> cb, Domain domain,
               ::std::uint32_t requiredFlags)
         {
            size_t n = 0;

            ::ifaddrs* addrs = nullptr;
            if (::getifaddrs(&addrs) != 0) {
               throw SystemError("Failed to access the network interfaces");
            }
            const Finally finally([addrs] {::freeifaddrs(addrs);});

            for (::ifaddrs* cur = addrs; cur; cur = cur->ifa_next) {
               if (cur->ifa_name == nullptr || cur->ifa_addr == nullptr) {
                  continue;
               }
               size_t addrLen = getAddressLength(cur->ifa_addr->sa_family);
               if (addrLen == 0) {
                  continue;
               }

               // check for supported domains
               ++n;
               Network nw(cur->ifa_name);
               nw._flags = toNetworkFlags(cur->ifa_flags);
               // check for required flags
               if ((nw._flags & requiredFlags) != requiredFlags) {
                  continue;
               }

               if (domain.id() != 0 && Domain(cur->ifa_addr->sa_family) != domain) {
                  continue;
               }

               {
                  auto len = getAddressLength(cur->ifa_addr->sa_family);
                  nw._address = Endpoint(
                        Address(SocketType(), Protocol(), len,
                              reinterpret_cast< const sockaddr_storage&>(*cur->ifa_addr)));
               }

               if (nw.testFlags(BROADCAST) && cur->ifa_broadaddr) {
                  if (cur->ifa_broadaddr->sa_family != cur->ifa_addr->sa_family) {

                     LogEntry(log()).debugging()
                           << "Broadcast address family does not match network address for network " << nw._interface
                           << doLog;
                  }

                  auto len = getAddressLength(cur->ifa_broadaddr->sa_family);
                  nw._broadcast = Endpoint(
                        Address(SocketType(), Protocol(), len,
                              reinterpret_cast< const sockaddr_storage&>(*cur->ifa_broadaddr)));

               }
               if (nw.testFlags(POINT_TO_POINT) && cur->ifa_dstaddr) {
                  if (cur->ifa_dstaddr->sa_family != cur->ifa_addr->sa_family) {
                     LogEntry(log()).debugging()
                           << "Point-to-point address family does not match network address for network "
                           << nw._interface << doLog;
                  }
                  auto len = getAddressLength(cur->ifa_dstaddr->sa_family);
                  nw._point2point = Endpoint(
                        Address(SocketType(), Protocol(), len,
                              reinterpret_cast< const sockaddr_storage&>(*cur->ifa_dstaddr)));
               }

               if (cur->ifa_netmask) {
                  nw._networkPrefix = netmask2prefix(*cur->ifa_netmask);
               }

               cb(nw);
            }
            return n;
         }

         Network::Prefix Network::netmask2prefix(const sockaddr& addr)
         {
            if (addr.sa_family == PF_INET) {
               const sockaddr_in& inet = reinterpret_cast< const sockaddr_in&>(addr);
               return ::canopy::countOnes(inet.sin_addr.s_addr);
            }
            return 0;
         }

      }
   }
}

::std::ostream& operator<<(::std::ostream& out, const ::canopy::io::net::Network& net)
{
   out << net.interface();
   out << "[ ";
   if (net.testFlags(::canopy::io::net::Network::UP)) {
      out << "UP ";
   }
   if (net.testFlags(::canopy::io::net::Network::RUNNING)) {
      out << "RUNNING ";
   }
   if (net.testFlags(::canopy::io::net::Network::LOOPBACK)) {
      out << "LOOPBACK ";
   }
   if (net.testFlags(::canopy::io::net::Network::MULTICAST)) {
      out << "MULTICAST ";
   }
   if (net.testFlags(::canopy::io::net::Network::BROADCAST)) {
      out << "BROADCAST ";
   }
   if (net.testFlags(::canopy::io::net::Network::POINT_TO_POINT)) {
      out << "POINT-TO-POINT ";
   }
   out << "] ";

   out << "type : " << net.endpoint().address().domain();

   {
      auto id = net.endpoint().address().nodeID();
      if (!id.empty()) {
         out << ", address " << net.endpoint().address().nodeID() << "/" << net.getPrefix();
      }
   }
   {
      auto addr = net.getBroadcastAddress();
      if (!addr.empty()) {
         auto id = addr.address().nodeID();
         if (!id.empty()) {
            out << ", broadcast " << id;
         }
      }
   }
   {
      auto addr = net.getPointToPointAddress();
      if (!addr.empty()) {
         auto id = addr.address().nodeID();
         if (!id.empty()) {
            out << ", point2point " << id;
         }
      }
   }
   return out;
}
