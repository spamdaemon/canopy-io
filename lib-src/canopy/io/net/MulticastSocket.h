#ifndef _CANOPY_IO_NET_MULTICASTSOCKET_H
#define _CANOPY_IO_NET_MULTICASTSOCKET_H

#ifndef _CANOPY_IO_NET_PROXY_H
#include <canopy/io/net/Proxy.h>
#endif

#ifndef _CANOPY_IO_NET_ENDPOINT_H
#include <canopy/io/net/Endpoint.h>
#endif

namespace canopy {
   namespace io {
      namespace net {

         /**
          * This class provides an implementation of multicast sockets.
          */
         class MulticastSocket : public Proxy
         {
               MulticastSocket(const MulticastSocket&) = delete;
               MulticastSocket&operator=(const MulticastSocket&) = delete;

               enum class Version
               {
                  V4, V6
               };

               /**
                * Constructor with a socket
                * @param socket a socket
                * @param options MC options
                * @throws SocketException if the socket could not be set up
                */
            private:
               MulticastSocket(::std::unique_ptr< Socket>&& socket);

               /**
                * Open a new Datagram socket for multicast.
                * @param d the socket domain
                * @param p the protocol
                * @throws a socket exception if the socket could not be opened
                */
            public:
               MulticastSocket(const Domain& d = INET_4, const Protocol& p = Protocol());

               /**
                * Destructor.
                */
            public:
               ~MulticastSocket();

               /**
                * Create a multicast socket.
                * @param group a multicast group
                * @return a pointer to a multicast socket
                * @throws SocketException if the socket could not be created
                */
            public:
               static ::std::unique_ptr< MulticastSocket> joinGroup(const Address& group, bool reuseAddr = true);

               /**
                * Create a multicast socket.
                * @param group a multicast group
                * @return a pointer to a multicast socket
                * @throws SocketException if the socket could not be created
                */
            public:
               static ::std::unique_ptr< MulticastSocket> joinGroup(const Endpoint& group, bool reuseAddr = true);

               /**
                * Create a multicast socket that is connected to the given multicast address.
                * @param group multicast address and port
                * @return a pointer to a multicast socket
                * @throws SocketException if the socket could not be created
                */
            public:
               static ::std::unique_ptr< MulticastSocket> createConnectedSocket(const Address& group);

               /**
                * Create a multicast socket that is connected to the given multicast address.
                * @param group multicast address and port
                * @return a pointer to a multicast socket
                * @throws SocketException if the socket could not be created
                */
            public:
               static ::std::unique_ptr< MulticastSocket> createConnectedSocket(const Endpoint& group);

               /**
                * Join the specified multicast address.
                * @param address an endpoint
                * @throws  a socket exception if the the socket could not join the given address
                */
            public:
               void join(const Address& address);

               /**
                * Leave the current multicast group.
                * @param address
                * @throws  a socket exception if the the socket could not join the given address
                */
            public:
               void leave(const Address& address);

               /**
                * Enable looping on the multicast connection.
                * This option is enabled by default.
                * @param enable enable loop on the multicast connection
                */
            public:
               void setMulticastLoopEnabled(bool enable);

               /**
                * Determine if multicast loop is enabled.
                * @return true if multicast loop is enabled
                */
            public:
               bool isMulticastLoopEnabled() const;

               /**
                * Set the multicast ttl or hops.
                * @param ttl the new ttl/hop value
                * @throws SocketException on error
                */
            public:
               void setMulticastTTL(::std::uint32_t ttl);

               /**
                * Get the multicast TTL.
                * @return the multicast ttl
                */
            public:
               ::std::uint32_t getMulticastTTL() const;

               /**
                * Map a domain to a version.
                * @param d a domain
                * @return a version
                * @throws SocketException if the domain is a valid version
                */
            private:
               static Version version(Domain d);

               /**
                * The IP version
                */
            private:
               Version _version;
         }
         ;
      }
   }
}
#endif
