#include <canopy/io/net/MulticastSocket.h>
#include <canopy/io/net/PosixSocket.h>
#include <canopy/io/net/Socket.h>

namespace canopy {
   namespace io {
      namespace net {
         Socket::~Socket()
         {
         }

         ::std::unique_ptr< Socket> Socket::createConnectedSocket(const Address& addr)
         {
            ::std::unique_ptr< Socket> s;
            if (addr.isMulticastAddress()) {
               s = MulticastSocket::createConnectedSocket(addr);
            }
            else {
               s.reset(new PosixSocket(addr.type(), addr.domain(), addr.protocol()));
               s->connect(addr);
            }
            return s;
         }

         ::std::unique_ptr< Socket> Socket::createBoundSocket(const Address& addr, bool reuseAddr)
         {
            ::std::unique_ptr< Socket> s;
            if (addr.isMulticastAddress()) {
               s = MulticastSocket::joinGroup(addr, reuseAddr);
            }
            else {
               s.reset(new PosixSocket(addr.type(), addr.domain(), addr.protocol()));
               s->setReuseAddressEnabled(reuseAddr);
               s->bind(addr);
            }
            return s;
         }

      }
   }
}
