#include <canopy/io/net/ConnectionRefused.h>

namespace canopy {
   namespace io {
      namespace net {
         ConnectionRefused::ConnectionRefused()
               : SocketException(CONNECTION_REFUSED)
         {
         }

         ConnectionRefused::~ConnectionRefused() throw()
         {
         }
      }
   }
}
