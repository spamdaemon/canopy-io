#include <canopy/io/net/NetException.h>
#include <memory>

namespace canopy {
   namespace io {
      namespace net {

         NetException::NetException(::std::string s)

               : ::canopy::io::IOException(::std::move(s))
         {
         }

         NetException::~NetException()
         {
         }
      }
   }
}
