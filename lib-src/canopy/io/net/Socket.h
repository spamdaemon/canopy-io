#ifndef _CANOPY_IO_NET_SOCKET_H
#define _CANOPY_IO_NET_SOCKET_H

#ifndef _CANOPY_IO_NATIVEFILEDESCRIPTOR_H
#include <canopy/io/NativeFileDescriptor.h>
#endif

#ifndef _CANOPY_IO_NET_H
#include <canopy/io/net/net.h>
#endif

#ifndef _CANOPY_IO_NET_ADDRESS_H
#include <canopy/io/net/Address.h>
#endif

#include <memory>

namespace canopy {
   namespace io {
      namespace net {

         /**
          * This is the baseclass for all socket implementations
          * that are based on a SocketDescriptor. The socket descriptor
          * is accessible via the protected method descriptor().
          * Each instance of this class is given a name, which can be
          * retrieved with the name() method. Basic operations on sockets
          * are provided by this class, such as write(), read(), close(), etc.
          *
          * @note Sockets are reference counted in a thread-safe way, so that they may
          *       be passed around threads.
          */
         class Socket : public ::canopy::io::NativeFileDescriptor
         {
               Socket(const Socket&) = delete;
               Socket&operator=(const Socket&) = delete;

               /** The socket descriptor */
            public:
               typedef Descriptor SocketDescriptor;

               /** The default constructor */
            public:
               inline Socket()
               {
               }

               /**
                * Destroy this socket reference. If this is the last
                * reference to the socket, then it is destroyed.
                * If the socket cannot be destroyed properly, then an error
                * message is printed to cerr
                */
            public:
               virtual ~Socket();

               /**
                * Create a socket that is connected to the specified address.
                * @param address an address
                * @return a connected socket
                * @throws socket exception if the socket could not be created
                */
            public:
               static ::std::unique_ptr< Socket> createConnectedSocket(const Address& addr);

               /**
                * Create a socket that is bound to the specified address.
                * @param address an address
                * @param reuseAddr reuse the address
                * @return a bound socket
                * @throws socket exception if the socket could not be created
                */
            public:
               static ::std::unique_ptr< Socket> createBoundSocket(const Address& addr, bool reuseAddr = true);

               /**
                * Get the id for this socket.
                * @note the id is not constant for the lifetime of this socket.
                * Calling close() on the socket will change the id.
                * @return the unique id.
                */
            public:
               virtual ::std::int32_t id() const = 0;

               /**
                * Get the socket type.
                * @return the type of this socket
                * @throw SocketException if the type could not be established
                */
            public:
               virtual SocketType type() const = 0;

               /**
                * Get the socket domain.
                * @return the domain of this socket
                * @throw SocketException if the domain could not be established
                */
            public:
               virtual Domain domain() const = 0;

               /**
                * Get the socket protocol.
                * @return the protocol used by this socket.
                * @throw SocketException if the domain could not be established
                */
            public:
               virtual Protocol protocol() const = 0;

               /**
                * Shut this socket down for any writes.
                */
            public:
               virtual void shutdownWrite() = 0;

               /**
                * Shut this socket down for any more reads.
                */
            public:
               virtual void shutdownRead() = 0;

               /**
                * @name UDP socket operations
                * @{
                */

               /**
                * Receive data on this socket. This method must be called
                * on an unconnected UDP socket. The address of the sender is
                * returned in the address.<p>
                * Even if blocking is enabled, this call may still return 0 if the socket
                * itself has been set to be non-blocking!
                * @param buffer the data buffer to be written
                * @param bufSize the number of characters in the buffer to write
                * @param from the source address of the data
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception SocketException if an error occurred
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               virtual ssize_t readFrom(char* buffer, size_t bufSize, Address& from, const Timeout& timeout) = 0;

               /**
                * Write a number of bytes via this socket to the specified destination.
                * This method must be called on an unconnected UDP socket.
                * @note if this is a UDP socket and the destination does not exist, then
                *       a CONNECTION_REFUSED exception may be raised.
                * @param buffer the data buffer to be written
                * @param bufSize the number of characters in the buffer to write
                * @param to the destination address
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes written or 0 if nothing could be written.
                * @exception SocketException if an error occurred
                * @exception InterruptedException if the call was interrupted
                * @exception ConnectionRefused may be thrown if the destination does not exist
                */
            public:
               virtual size_t writeTo(const char* buffer, size_t bufSize, const Address& to,
                     const Timeout& timeout) = 0;

               /*@}*/

               /**
                * Determine if this socket is a server socket. A server socket is one
                * for which listen() has been invoked.
                * @return true if listen has been invoked
                * @exception SocketException if an error occurred
                */
            public:
               virtual bool isServerSocket() const = 0;

               /**
                * Set the send buffer size
                * @param size the send buffer size
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual void setSendBufferSize(::std::uint32_t size) = 0;

               /**
                * Get the receive buffer size.
                * @return size of the receive buffer
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual ::std::uint32_t getSendBufferSize() const = 0;

               /**
                * Set the receive buffer size.
                * @param size the receive buffer size
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual void setReceiveBufferSize(::std::uint32_t size) = 0;

               /**
                * Get the receive buffer size.
                * @return size of the receive buffer
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual ::std::uint32_t getReceiveBufferSize() const = 0;

               /**
                * Clear any errors on this socket.
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual void clearErrors() = 0;

               /**
                * Enable reuse of addresses if there are no connected sockets.
                * @param enabled true if the address can be bound again
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual void setReuseAddressEnabled(bool enabled) = 0;

               /**
                * Test if the address reuse option has been enabled
                * @return true if addresses can be reused when binding
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual bool isReuseAddressEnabled() const = 0;

               /**
                * Enable broadcast for this socket. This option is only
                * available for UDP sockets.
                * @param enabled true if this socket should be able to broadcast
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual void setBroadcastEnabled(bool enabled) = 0;

               /**
                * Test if this socket is capable of broadcasting
                * @return true if this socket can broadcast
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual bool isBroadcastEnabled() const = 0;

               /**
                * Enable  sending  of  keep-alive messages on
                * connection-oriented sockets.
                * @param enabled true if keep alive message are to be sent
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual void setKeepAliveEnabled(bool enabled) = 0;

               /**
                * Test if the keep alive option
                * is enabled.
                * @return true if the keep alive option is enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual bool isKeepAliveEnabled() const = 0;

               /**
                * Enable lingering of this socket after closing it.
                * @param duration the time in seconds that the socket should remain
                * open after it has been closed
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual void enableLinger(::std::uint32_t duration) = 0;

               /**
                * Disable linger.
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual void disableLinger() = 0;

               /**
                * Get the linger duration
                * @return the linger time or 0 if linger is not enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual ::std::uint32_t getLingerTime() const = 0;

               /**
                * Test if linger is enabled.
                * @return true if lingering for this socket is enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               virtual bool isLingerEnabled() const = 0;

               /*@}*/

               /**
                * @name BSD socket operations.
                * @{
                */

               /**
                * Put this socket in listening mode. Sockets of type
                * UDP do not support this method.
                * @param backlog the number connection requrest that will be
                * queued up before clients receive a "conncection refused"
                * @exception SocketException if this socket does support this method
                * or some other error occurred.
                */
            public:
               virtual void listen(::std::uint32_t backlog) = 0;

               /**
                * Accept a connection on this socket. The current
                * thread is suspended until a connection is made. Clients
                * are responsible for deleting the returned object.
                * Sockets of type UDP do not support this method.
                * @param peer an address which will contain the peer address upon completion or nullptr if the peer address is not needed
                * @return a socket representing the connection to
                * a remote host, or 0 if no connection was made
                * @throw SocketException if some error occurred or this method
                * is not supported by this socket.
                */
            public:
               virtual ::std::unique_ptr< Socket> accept(Address* peer = nullptr) = 0;

               /**
                * Bind this socket to the specified end-point address. For
                * obvious reasons, the endpoint address must be describe the
                * current host node.
                * @param address an end-point address.
                * @exception SocketException if the socket could not be bound
                * to the address and port
                */
            public:
               virtual void bind(const Address& address) = 0;

               /**
                * Connect this socket to an endpoint at the specified address. If this socket
                * is of type UDP, then no real connection is made, but all
                * datagrams are sent by default to the specified address.
                * @param address an address.
                * @return true if the socket is now connection, false if this socket is in non-blocking mode and the connection is in progress
                * @exception SocketException if the socket could not be connect
                * to the address and port
                * @exception ConnectionRefused if the connection failed
                */
            public:
               virtual bool connect(const Address& address) = 0;

               /**
                * @name Enquiring about this socket.
                * @{
                */

               /**
                * Get the address to which this socket is bound.
                * @return the inet address to which this socket is bound
                * @throw canopy::io::IOException if the socket is not bound
                */
            public:
               virtual Address getBoundAddress() const = 0;

               /**
                * Get the address of the peer socket.
                * @return the inet address of socket to which this socket is connected
                * @throw canopy::io::IOException if the socket is not connected
                */
            public:
               virtual Address getPeerAddress() const = 0;

               /*@}*/

         };
      }
   }
}
#endif
