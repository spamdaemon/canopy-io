#ifndef _CANOPY_IO_NET_ADDRESSEXCEPTION_H
#define _CANOPY_IO_NET_ADDRESSEXCEPTION_H

#ifndef _CANOPY_IO_NET_IOEXCEPTION_H
#include <canopy/io/net/NetException.h>
#endif

namespace canopy {
   namespace io {
      namespace net {

         /**
          * This exception may thrown by operations that work
          * no connected sockets.
          */
         class AddressException : public NetException
         {
            private:
               AddressException() = delete;

               /**
                * Create an exception with the specified error string.
                * The exception code is set to OTHER.
                * @param msg an error message
                */
            public:
               AddressException(::std::string msg);

               /** Destructor */
            public:
               ~AddressException() throw()
               {
               }

         };
      }
   }
}

#endif
