#ifndef _CANOPY_IO_NET_POSIXSOCKET_H
#define _CANOPY_IO_NET_POSIXSOCKET_H

#ifndef _CANOPY_IO_NET_SOCKET_H
#include <canopy/io/net/Socket.h>
#endif

#include <memory>
#include <atomic>

namespace canopy {
   namespace io {
      namespace net {

         /**
          * This is an implementation of a socket using the standard Posix / BSD api.
          * This implementation supports UDP and TCP sockets and any sockets that
          * can be created via this class.
          */
         class PosixSocket : public Socket
         {
               PosixSocket(const PosixSocket&) = delete;
               PosixSocket&operator=(const PosixSocket&) = delete;

               /**
                * Create a new socket with the specified socket descriptor.
                * @param d the socket's domain
                * @param t the socket's type
                * @param p the socket's protocol
                * @param sd the socket descriptor
                */
            private:
               explicit PosixSocket(const SocketType& t, const Domain& d, const Protocol& p, SocketDescriptor);

               /** The default constructor */
            public:
               PosixSocket();

               /**
                * Open a new socket.
                * @param d the socket domain
                * @param t the socket type
                * @param p the protocol
                * @throws a socket exception if the socket could not be opened
                */
            public:
               PosixSocket(const SocketType& t, const Domain& d, const Protocol& p = Protocol());

               /**
                * Destroy this socket reference. If this is the last
                * reference to the socket, then it is destroyed.
                * If the socket cannot be destroyed properly, then an error
                * message is printed to cerr
                */
            public:
               ~PosixSocket();

               /**
                * Get the id for this socket.
                * @note the id is not constant for the lifetime of this socket.
                * Calling close() on the socket will change the id.
                * @return the unique id.
                */
            public:
               inline ::std::int32_t id() const
               {
                  // use the socket descirptor as an id
                  return descriptor();
               }

               /**
                * Test if this is an invalid socket.
                * @return true if this socket is <em>invalid</em>
                */
            public:
               bool invalid() const
               {
                  return descriptor() == -1;
               }

               /**
                * Get the socket type.
                * @return the type of this socket
                * @throw SocketException if the type could not be established
                */
            public:
               SocketType type() const;

               /**
                * Get the socket domain.
                * @return the domain of this socket
                * @throw SocketException if the domain could not be established
                */
            public:
               Domain domain() const;

               /**
                * Get the socket protcol.
                * @return the protocol used by this socket.
                * @throw SocketException if the domain could not be established
                */
            public:
               Protocol protocol() const;

               /**
                * Close this socket.
                * @note this method will change the socket's id
                * @throws Exception if the socket could not be closed
                */
            public:
               void close();

               /**
                * Test if this socket has been closed. If this
                * method returns false, then this does not mean
                * that the next method invocation for this socket
                * will succeed.
                * @return true if this socket can no longer be used, false
                * if the socket currently not closed
                */
            public:
               bool isClosed() const;

               /**
                * Shut this socket down for any writes.
                */
            public:
               void shutdownWrite();

               /**
                * Shut this socket down for any more reads.
                */
            public:
               void shutdownRead();

               ::canopy::io::IOEvents block(const ::canopy::io::IOEvents& events, const Timeout& timeout) const;

               /**
                * Peek at the buffer in the socket. This method works just like read, but does not
                * consume the data on the socket. This method is ideal for checking if a socket has been
                * closed in an orderly fashion, as this method will return 0 in that case.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to read
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t peek(char* buffer, size_t bufSize, const Timeout& timeout) const;

               /**
                * Read a number of bytes from this socket. This method blocks
                * unless the socket is a non-blocking socket or there is already
                * data to be read. This method must return 0 to indicate that the socket
                * buffer is full.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to read
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception ConnectionException if an error occurred during reading
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t read(char* buffer, size_t bufSize, const Timeout& timeout);

               /**
                * Write a number of bytes to this socket. This method blocks
                * until all bytes have been written. If this socket is in non-blocking mode
                * and no bytes can currently be written (because the buffer is full), then 0 is returned.
                * Even if blocking is enabled, this call may still return 0 if the socket
                * itself has been set to be non-blocking!
                * @note if this is a UDP socket and the destination does not exist, then
                *       a CONNECTION_REFUSED exception may be raised.
                * @param buffer a buffer
                * @param bufSize the size of the buffer
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes written to the socket or 0 if nothing can be written at the moment
                * @exception SocketException if an error occurred during writing
                * @exception InterruptedException if the call was interrupted
                * @exception ConnectionRefused may be thrown if the destination does not exist
                */
            public:
               size_t write(const char* buffer, size_t bufSize, const Timeout& timeout);

               /**
                * @name UDP socket operations
                * @{
                */

               /**
                * Receive data on this socket. This method must be called
                * on an unconnected UDP socket. The address of the sender is
                * returned in the address.<p>
                * Even if blocking is enabled, this call may still return 0 if the socket
                * itself has been set to be non-blocking!
                * @param buffer the data buffer to be written
                * @param bufSize the number of characters in the buffer to write
                * @param from the source address of the data
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes read or -1 this socket has been closed, or 0 if there is currently no data
                * @exception SocketException if an error occurred
                * @exception InterruptedException if the call was interrupted
                * @note the number of bytes read will never exceed 0x7FFFFFFFF bytes, regardless of bufSize
                */
            public:
               ssize_t readFrom(char* buffer, size_t bufSize, Address& from, const Timeout& timeout);

               /**
                * Write a number of bytes via this socket to the specified destination.
                * This method must be called on an unconnected UDP socket.
                * @note if this is a UDP socket and the destination does not exist, then
                *       a CONNECTION_REFUSED exception may be raised.
                * @param buffer the data buffer to be written
                * @param bufSize the number of characters in the buffer to write
                * @param to the destination address
                * @param block if true (default), then this method blocks until there is at least 1 byte to write
                * @return the number of bytes written or 0 if nothing could be written.
                * @exception SocketException if an error occurred
                * @exception InterruptedException if the call was interrupted
                * @exception ConnectionRefused may be thrown if the destination does not exist
                */
            public:
               size_t writeTo(const char* buffer, size_t bufSize, const Address& to, const Timeout& timeout);

               /*@}*/

               /**
                * Determine if this socket is a server socket. A server socket is one
                * for which listen() has been invoked.
                * @return true if listen has been invoked
                * @exception SocketException if an error occurred
                */
            public:
               bool isServerSocket() const;

               /**
                * Set the socket to block on I/O operations.
                * @param enabled if true, then I/O operations may block
                * @exception SocketException if an error occurred
                */
            public:
               void setBlockingEnabled(bool enabled);

               /**
                * Test if blocking I/O is enabled.
                * @return true if I/O calls may be block, false otherwise
                * @exception SocketException if an error occurred
                */
            public:
               bool isBlockingEnabled() const;

               /**
                * Set the send buffer size
                * @param size the send buffer size
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setSendBufferSize(::std::uint32_t size);

               /**
                * Get the receive buffer size.
                * @return size of the receive buffer
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               ::std::uint32_t getSendBufferSize() const;

               /**
                * Set the receive buffer size.
                * @param size the receive buffer size
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setReceiveBufferSize(::std::uint32_t size);

               /**
                * Get the receive buffer size.
                * @return size of the receive buffer
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               ::std::uint32_t getReceiveBufferSize() const;

               /**
                * Clear any errors on this socket.
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void clearErrors();

               /**
                * Enable reuse of addresses if there are no connected sockets.
                * @param enabled true if the address can be bound again
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setReuseAddressEnabled(bool enabled);

               /**
                * Test if the address reuse option has been enabled
                * @return true if addresses can be reused when binding
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               bool isReuseAddressEnabled() const;

               /**
                * Enable broadcast for this socket. This option is only
                * available for UDP sockets.
                * @param enabled true if this socket should be able to broadcast
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setBroadcastEnabled(bool enabled);

               /**
                * Test if this socket is capable of broadcasting
                * @return true if this socket can broadcast
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               bool isBroadcastEnabled() const;

               /**
                * Enable  sending  of  keep-alive messages on
                * connection-oriented sockets.
                * @param enabled true if keep alive message are to be sent
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void setKeepAliveEnabled(bool enabled);

               /**
                * Test if the keep alive option
                * is enabled.
                * @return true if the keep alive option is enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               bool isKeepAliveEnabled() const;

               /**
                * Enable lingering of this socket after closing it.
                * @param duration the time in seconds that the socket should remain
                * open after it has been closed
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void enableLinger(::std::uint32_t duration);

               /**
                * Disable linger.
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               void disableLinger();

               /**
                * Get the linger duration
                * @return the linger time or 0 if linger is not enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               ::std::uint32_t getLingerTime() const;

               /**
                * Test if linger is enabled.
                * @return true if lingering for this socket is enabled
                * @exception SocketException if this operation could not
                * be performed on this socket.
                */
            public:
               bool isLingerEnabled() const;

               /*@}*/

               /**
                * @name BSD socket operations.
                * @{
                */

               /**
                * Put this socket in listening mode. Sockets of type
                * UDP do not support this method.
                * @param backlog the number connection requrest that will be
                * queued up before clients receive a "conncection refused"
                * @exception SocketException if this socket does support this method
                * or some other error occurred.
                */
            public:
               void listen(::std::uint32_t backlog);

               /**
                * Accept a connection on this socket. The current
                * thread is suspended until a connection is made. Clients
                * are responsible for deleting the returned object.
                * Sockets of type UDP do not support this method.
                * @param peer an address which will contain the peer address upon completion.
                * @return a socket representing the connection to
                * a remote host, or nullptr if no connection was made
                * @throw SocketException if some error occurred or this method
                * is not supported by this socket.
                */
            public:
               ::std::unique_ptr< Socket> accept(Address* peer);

               /**
                * Bind this socket to the specified end-point address. For
                * obvious reasons, the endpoint address must be describe the
                * current host node.
                * @param address an end-point address.
                * @exception SocketException if the socket could not be bound
                * to the address and port
                */
            public:
               void bind(const Address& address);

               /**
                * Connect this socket to an endpoint at the specified address. If this socket
                * is of type UDP, then no real connection is made, but all
                * datagrams are sent by default to the specified address.
                * @param address an address.
                * @return true if the socket was connected, false if the socket is non-blocking and a connection is in-progress
                * @exception SocketException if the socket could not be connect
                * to the address and port
                * @exception ConnectionRefused if the connection failed
                */
            public:
               bool connect(const Address& address);

               /*@}*/

               /**
                * @name Enquiring about this socket.
                * @{
                */

               /**
                * Get the address to which this socket is bound.
                * @return the inet address to which this socket is bound
                * @throw canopy::io::IOException if the socket is not bound
                */
            public:
               Address getBoundAddress() const;

               /**
                * Get the address of the peer socket.
                * @return the inet address of socket to which this socket is connected
                * @throw canopy::io::IOException if the socket is not connected
                */
            public:
               Address getPeerAddress() const;

               /*@}*/

               /**
                * Get the descriptor
                * @return the descriptor
                */
            public:
               SocketDescriptor descriptor() const
               {
                  return _sd.load();
               }

               /** The descriptor itself */
            private:
               mutable ::std::atomic< SocketDescriptor> _sd;

               /** The socket domain */
            private:
               Domain _domain;
               /** The socket type */
            private:
               SocketType _type;
               /** The socket protocol */
            private:
               Protocol _protocol;

         };
      }
   }
}
#endif
