#include <canopy/io/net/net.h>
#include <canopy/io/net/PosixSocket.h>
#include <canopy/io/net/Proxy.h>
#include <iostream>
#include <cassert>

using namespace ::std;

namespace canopy {
   namespace io {
      namespace net {

         Proxy::Proxy(::std::unique_ptr< ::canopy::io::net::Socket> socket)
               : _socket(::std::move(socket))
         {
            if (!_socket) {
               throw ::std::invalid_argument("Null pointer");
            }
         }

         Proxy::~Proxy()
         {
         }

         void Proxy::close()
         {
            _socket->close();
         }

         bool Proxy::isClosed() const
         {
            return _socket->isClosed();
         }
         ::canopy::io::IOEvents Proxy::block(const ::canopy::io::IOEvents& events, const Timeout& timeout) const
         {
            return _socket->block(events, timeout);
         }

         void Proxy::shutdownWrite()
         {
            _socket->shutdownWrite();
         }

         void Proxy::shutdownRead()
         {
            _socket->shutdownRead();
         }

         size_t Proxy::writeTo(const char* buffer, size_t bufSize, const Address& to, const Timeout& timeout)
         {
            return _socket->writeTo(buffer, bufSize, to, timeout);
         }

         ssize_t Proxy::readFrom(char* buffer, size_t bufSize, Address& from, const Timeout& timeout)
         {
            return _socket->readFrom(buffer, bufSize, from, timeout);
         }

         ssize_t Proxy::peek(char* buffer, size_t bufSize, const Timeout& timeout) const
         {
            return _socket->peek(buffer, bufSize, timeout);
         }

         ssize_t Proxy::read(char* buffer, size_t bufSize, const Timeout& timeout)
         {
            return _socket->read(buffer, bufSize, timeout);
         }

         size_t Proxy::write(const char* buffer, size_t bufSize, const Timeout& timeout)
         {
            return _socket->write(buffer, bufSize, timeout);
         }

         SocketType Proxy::type() const
         {
            return _socket->type();
         }

         Domain Proxy::domain() const
         {
            return _socket->domain();
         }

         Protocol Proxy::protocol() const
         {
            return _socket->protocol();
         }

         void Proxy::setBlockingEnabled(bool enabled)
         {
            _socket->setBlockingEnabled(enabled);
         }

         bool Proxy::isBlockingEnabled() const
         {
            return _socket->isBlockingEnabled();
         }

         void Proxy::setSendBufferSize(::std::uint32_t size)
         {
            _socket->setSendBufferSize(size);
         }

         ::std::uint32_t Proxy::getSendBufferSize() const
         {
            return _socket->getSendBufferSize();
         }

         void Proxy::setReceiveBufferSize(::std::uint32_t size)
         {
            _socket->setReceiveBufferSize(size);
         }

         ::std::uint32_t Proxy::getReceiveBufferSize() const
         {
            return _socket->getReceiveBufferSize();
         }

         void Proxy::clearErrors()
         {
            _socket->clearErrors();
         }

         bool Proxy::connect(const Address& addr)
         {
            return _socket->connect(addr);
         }

         void Proxy::bind(const Address& addr)
         {
            _socket->bind(addr);
         }

         bool Proxy::isServerSocket() const
         {
            return _socket->isServerSocket();
         }

         void Proxy::setReuseAddressEnabled(bool enabled)
         {
            _socket->setReuseAddressEnabled(enabled);
         }

         bool Proxy::isReuseAddressEnabled() const
         {
            return _socket->isReuseAddressEnabled();
         }

         void Proxy::setBroadcastEnabled(bool enabled)
         {
            _socket->setBroadcastEnabled(enabled);
         }

         bool Proxy::isBroadcastEnabled() const
         {
            return _socket->isBroadcastEnabled();
         }

         void Proxy::setKeepAliveEnabled(bool enabled)
         {
            _socket->setKeepAliveEnabled(enabled);
         }

         bool Proxy::isKeepAliveEnabled() const
         {
            return _socket->isKeepAliveEnabled();
         }

         void Proxy::enableLinger(::std::uint32_t duration)
         {
            _socket->enableLinger(duration);
         }

         void Proxy::disableLinger()
         {
            _socket->disableLinger();
         }

         ::std::uint32_t Proxy::getLingerTime() const
         {
            return _socket->getLingerTime();
         }

         bool Proxy::isLingerEnabled() const
         {
            return _socket->isLingerEnabled();
         }

         Address Proxy::getBoundAddress() const
         {
            return _socket->getBoundAddress();
         }

         Address Proxy::getPeerAddress() const
         {
            return _socket->getPeerAddress();
         }

         void Proxy::listen(::std::uint32_t backlog)
         {
            _socket->listen(backlog);
         }

         ::std::unique_ptr< Socket> Proxy::accept(Address* peer)
         {
            return _socket->accept(peer);
         }
      }
   }
}
