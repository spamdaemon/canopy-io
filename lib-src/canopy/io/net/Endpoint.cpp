#include <canopy/io/net/Endpoint.h>
#include <canopy/os/SystemError.h>
#include <canopy/os/ResourceUnavailable.h>
#include <canopy/io/net/MulticastSocket.h>
#include <canopy/io/net/PosixSocket.h>
#include <canopy/os/InterruptedException.h>
#include <canopy/io/net/SocketException.h>
#include <canopy/io/net/ConnectionRefused.h>
#include <canopy/common.h>
#include <netdb.h>
#include <strings.h>
#include <sys/socket.h>
#include <algorithm>
#include <cassert>
#include <cstring>
#include <functional>
#include <iostream>
#include <sstream>

using namespace ::std;
using namespace ::canopy::os;

namespace canopy {
   namespace io {
      namespace net {

         Endpoint::~Endpoint()
         {
         }

         Endpoint::Endpoint()
         {
         }

         Endpoint::Endpoint(const Address& addr)
         {
            _addresses.reserve(1);
            _addresses.push_back(addr);
         }

         Endpoint::Endpoint(const string& node, const char* service, SocketType t, Domain d, Protocol p)
         {
            lookupAddresses(node.c_str(), service, t, d, p, false);
         }

         Endpoint::Endpoint(const string& node, ::std::uint32_t service, SocketType t, Domain d, Protocol p)
         {
            lookupAddresses(node.c_str(), service, t, d, p, false);
         }

         Endpoint::Endpoint(const HostPort& hostport, SocketType t, Domain d, Protocol p)
               : Endpoint(hostport.host, hostport.port, t, d, p)
         {
         }

         Endpoint::Endpoint(const NodeService& nodeservice, SocketType t, Domain d, Protocol p)
               : Endpoint(nodeservice.node, nodeservice.service.c_str(), t, d, p)
         {
         }

         Endpoint Endpoint::filter(SocketType t, Domain d, Protocol p) const
         {
            Endpoint ep;
            for (const Address& addr : _addresses) {
               if (t.id() != 0 && addr.type() != t) {
                  continue;
               }
               if (d.id() != 0 && addr.domain() != d) {
                  continue;
               }
               if (p.id() != 0 && addr.protocol() != p) {
                  continue;
               }
               ep._addresses.push_back(addr);
            }
            return ep;
         }

         Address Endpoint::address() const
         {
            if (_addresses.empty()) {
               throw ::std::runtime_error("Endpoint has no addresses");
            }
            return _addresses[0];
         }

         Endpoint Endpoint::createEndpoint(const char* hostport, const char* defService, SocketType t, Domain d,
               Protocol p)
         {
            // find a colon
            const char* sep = hostport == nullptr ? (const char*) 0 : rindex(hostport, ':');

            ::std::uint32_t port;
            const char* service = nullptr;
            if (sep == nullptr || *(sep + 1) == '\0') {
               // use the default port
               service = defService;
               if (defService == nullptr || strlen(defService) == 0) {
                  throw ::std::invalid_argument("No port specified and no default service known");
               }
            }
            else {
               // assign the port string
               ::std::string tstr(sep + 1);
               ::std::istringstream in(tstr);
               in >> port;
               if (in.fail() || !in.eof()) {
                  throw ::std::invalid_argument("Invalid port " + tstr);
               }
            }

            string host;
            if (hostport) {
               if (sep) {
                  host.assign(hostport, sep - hostport);
               }
               else {
                  host = hostport;
               }
            }
            if (host.empty()) {
               host = "127.0.0.1";
            }
            if (service != nullptr) {
               return Endpoint(host, service, t, d, p);
            }
            else {
               return Endpoint(host, port, t, d, p);
            }
         }

         Endpoint Endpoint::createEndpoint(const char* hostport, ::std::uint32_t defPort, SocketType t, Domain d,
               Protocol p)
         {
            // find a colon
            const char* sep = hostport == nullptr ? nullptr : rindex(hostport, ':');

            ::std::uint32_t port;
            if (sep == nullptr || *(sep + 1) == '\0') {
               // use the default port
               port = defPort;
            }
            else {
               // assign the port string
               ::std::string tstr(sep + 1);
               ::std::istringstream in(::std::string(sep + 1));
               in >> port;
               if (in.fail() || !in.eof()) {
                  throw ::std::invalid_argument("Invalid port " + tstr);
               }
            }

            string host;
            if (hostport) {
               if (sep) {
                  host.assign(hostport, sep - hostport);
               }
               else {
                  host = hostport;
               }
            }
            if (host.empty()) {
               host = "127.0.0.1";
            }
            return Endpoint(host, port, t, d, p);
         }

         Endpoint Endpoint::createEndpoint(const char* hostport, SocketType t, Domain d, Protocol p)
         {
            // call createEndpoint with an incorrect default service
            return createEndpoint(hostport, nullptr, t, d, p);
         }

         Endpoint Endpoint::createAnyEndpoint(const char* service, SocketType t, Domain d, Protocol p)
         {
            Endpoint res;
            res.lookupAddresses(nullptr, service, t, d, p, true);
            return res;
         }

         Endpoint Endpoint::createAnyEndpoint(::std::uint32_t service, SocketType t, Domain d, Protocol p)
         {
            Endpoint res;
            res.lookupAddresses(nullptr, service, t, d, p, true);
            return res;
         }

         Endpoint Endpoint::createLoopbackEndpoint(const char* service, SocketType t, Domain d, Protocol p)
         {
            Endpoint res;
            res.lookupAddresses(nullptr, service, t, d, p, false);
            return res;
         }

         Endpoint Endpoint::createLoopbackEndpoint(::std::uint32_t service, SocketType t, Domain d, Protocol p)
         {
            Endpoint res;
            res.lookupAddresses(nullptr, service, t, d, p, false);
            return res;
         }

         void Endpoint::lookupAddresses(const char* node, ::std::uint32_t service, SocketType t, Domain d, Protocol p,
               bool bindable)
         {
            ostringstream str;
            str << service;
            lookupAddresses(node, str.str().c_str(), t, d, p, bindable);
         }

         void Endpoint::lookupAddresses(const char* node, const char* service, SocketType t, Domain d, Protocol p,
               bool bindable)
         {
            if (node == nullptr && service == nullptr) {
               throw ::std::invalid_argument("At least one of node or service must be specified");
            }

            struct ::addrinfo hints;
            struct ::addrinfo* info = nullptr;
            memset(&hints, 0, sizeof(struct ::addrinfo));

            hints.ai_flags = AI_ALL;
            if (bindable) {
               hints.ai_flags |= AI_PASSIVE;
            }
            hints.ai_family = d.id();
            hints.ai_socktype = t.id();
            hints.ai_protocol = p.id();

            int errCode = ::getaddrinfo(node, service, &hints, &info);
            if (errCode != 0) {
               string errorString = ::gai_strerror(errCode);
               errorString += " : ";
               errorString += node;

               switch (errCode) {
                  case EAI_NONAME:
                     throw AddressException(errorString);
                  case EAI_SERVICE:
                     throw AddressException(errorString);
                  case EAI_AGAIN:
                     throw ResourceUnavailable(errorString, true);
                  case EAI_ADDRFAMILY:
                     throw AddressException(errorString);
                  case EAI_SYSTEM:
                     throw SystemError(errorString);
                  default:
                     throw AddressException(errorString);
               }
            }

            const Finally finally([info] {::freeaddrinfo(info);});

            // for now, use the first address that pops up; we actually need a way to connect
            // using all possible addresses that mapped to the name

            for (struct ::addrinfo* i = info; i != 0; i = i->ai_next) {
               assert(i->ai_addr->sa_family == i->ai_family && "Socket families to do not match");
               // this is a safe transform
               const ::sockaddr_storage* saddr = reinterpret_cast< const ::sockaddr_storage*>(i->ai_addr);
               Address addr(SocketType(i->ai_socktype), Protocol(i->ai_protocol), i->ai_addrlen, *saddr);
               _addresses.push_back(::std::move(addr));
            }
         }

         void Endpoint::connect(::canopy::io::net::Socket& s) const
         {
            const Endpoint xep = filter(s.type(), s.domain(), s.protocol());
            for (const Address& addr : xep) {
               try {
                  s.connect(addr);
                  return;
               }
               catch (const SocketException& e) {
                  ::std::cerr << e.what() << ::std::endl;
               }
            }
            throw SocketException("Endpoint has no usable addresses");
         }

         void Endpoint::bind(::canopy::io::net::Socket& s) const
         {
            const Endpoint xep = filter(s.type(), s.domain(), s.protocol());
            for (const Address& addr : xep) {
               try {
                  s.bind(addr);
                  return;
               }
               catch (const SocketException&) {
               }
            }
            throw SocketException("Endpoint has no usable addresses");
         }

         ::std::unique_ptr< ::canopy::io::net::Socket> Endpoint::createConnectedSocket() const
         {
            for (const Address& addr : _addresses) {
               try {
                  return ::canopy::io::net::Socket::createConnectedSocket(addr);
               }
               catch (const SocketException&) {
                  // check the next address
               }
            }

            throw SocketException("Endpoint has no usable addresses");
         }

         ::std::unique_ptr< ::canopy::io::net::Socket> Endpoint::createBoundSocket(bool reuseAddr) const
         {
            for (const Address& addr : _addresses) {
               try {
                  return ::canopy::io::net::Socket::createBoundSocket(addr,reuseAddr);
               }
               catch (const SocketException&) {
                  // check the next address
               }
            }
            // loop over all addresses for the endpoint, until we find one which can be bound
            throw SocketException("Endpoint has no usable addresses");
         }

      }
   }
}
::std::ostream& operator<<(::std::ostream& out, const ::canopy::io::net::Endpoint& ep)
{
   out << "Endpoint [ " << ::std::endl;
   for (auto i : ep) {
      out << i << ::std::endl;
   }
   out << "]" << ::std::endl;
   return out;
}

