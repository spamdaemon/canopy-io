#ifndef _CANOPY_IO_NET_CONNECTIONREFUSED_H
#define _CANOPY_IO_NET_CONNECTIONREFUSED_H

#ifndef _CANOPY_IO_NET_SOCKETEXCEPTION_H
#include <canopy/io/net/SocketException.h>
#endif

namespace canopy {
   namespace io {
      namespace net {

         /**
          * This exception may thrown by operations that work
          * no connected sockets.
          */
         class ConnectionRefused : public SocketException
         {
               /** Default constructor */
            public:
               ConnectionRefused();

               /** Destructor */
            public:
               ~ConnectionRefused() throw();
         };
      }
   }
}

#endif
