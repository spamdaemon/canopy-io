#ifndef _CANOPY_IO_NET_ENDPOINT_H
#define _CANOPY_IO_NET_ENDPOINT_H

#ifndef _CANOPY_IO_NET_ADDRESSEXCEPTION_H
#include <canopy/io/net/AddressException.h>
#endif

#ifndef _CANOPY_IO_NET_H
#include <canopy/io/net/net.h>
#endif

#ifndef _CANOPY_IO_NET_ADDRESS_H
#include <canopy/io/net/Address.h>
#endif

#ifndef _CANOPY_IO_NET_SOCKET_H
#include <canopy/io/net/Socket.h>
#endif

#include <string>
#include <stdexcept>
#include <vector>
#include <memory>
#include <iosfwd>

namespace canopy {
   namespace io {
      namespace net {

         /**
          * An Endpoint is communications target which resides at some node and is reachable
          * via some service.
          *
          * @todo add a static method to access the primary domain address
          * @todo add a static method to access the broadcast address for an interface
          */
         class Endpoint
         {
               /** The iterator for the addresses */
            public:
               typedef ::std::vector< Address>::const_iterator const_iterator;

               /**
                * Create an end-point with no addresses.
                */
            public:
               Endpoint();

               /**
                * Create an end-point containing the specified address.
                * @param addr an address
                */
            public:
               Endpoint(const Address& addr);

               /**
                * Create an Endpoint for a node and service. The endpoint should be
                * reachable via a socket in the specified domain and type.
                * @param node the name of a node
                * @param service the name of a service (can be 0)
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ::std::exception if some kind of error occurred
                * @throws ResourceUnavailable if the endpoint cannot be looked up at the moment
                * @throws ::std::invalid_argument if node==0 && service==0
                */
            public:
               Endpoint(const ::std::string& node, const char* service = nullptr, SocketType t = SocketType(),
                     Domain d = Domain(), Protocol p = Protocol());

               /**
                * Create an Endpoint for a node and service. The endpoint should be
                * reachable via a socket in the specified domain and type.
                * @param node the name of a node
                * @param servicePort the service port (can be 0)
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ::std::exception if some kind of error occurred
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                */
            public:
               Endpoint(const ::std::string& node, ::std::uint32_t servicePort, SocketType t = SocketType(), Domain d =
                     Domain(), Protocol p = Protocol());

               /**
                * Create an Endpoint for a node and service. The endpoint should be
                * reachable via a socket in the specified domain and type.
                * @param hostport a tuple of host and port
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ::std::exception if some kind of error occurred
                * @throws ResourceUnavailable if the endpoint cannot be looked up at the moment
                * @throws ::std::invalid_argument if node==0 && service==0
                */
            public:
               Endpoint(const HostPort& hostport, SocketType t = SocketType(), Domain d = Domain(), Protocol p =
                     Protocol());

               /**
                * Create an Endpoint for a node and service. The endpoint should be
                * reachable via a socket in the specified domain and type.
                * @param nodeservice a tuple of node and service name
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ::std::exception if some kind of error occurred
                * @throws ResourceUnavailable if the endpoint cannot be looked up at the moment
                * @throws ::std::invalid_argument if node==0 && service==0
                */
            public:
               Endpoint(const NodeService& nodeservice, SocketType t = SocketType(), Domain d = Domain(), Protocol p =
                     Protocol());

               /**
                * Destroys this Endpoint object.
                */
            public:
               ~Endpoint();

               /**
                * Create an endpoint from a host-port string. The host-port string is a string
                * of the form @code [<node|address>][:<port>] @endcode. This constructor
                * simplifies the use of endpoints when using command line arguments.
                * If the node or address part are omitted, or hostport=0 or empty, then 127.0.0.1 is will be used.
                * If no port is specified, the the default service is assumed and must be specified.
                * @param hostport the host-port combination
                * @param defService the default service, if hostport does not contain a port number
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ::std::exception if some kind of error occurred
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                * @throws ::std::invalid_argument if hostport==0 || strlen(hostport)==0
                */
            public:
               static Endpoint createEndpoint(const char* hostport, const char* defService, SocketType t = SocketType(),
                     Domain d = Domain(), Protocol p = Protocol());

               /**
                * Create an endpoint from a host-port string. The host-port string is a string
                * of the form @code [<node|address>][:<port>] @endcode. This constructor
                * simplifies the use of endpoints when using command line arguments.
                * If the node or address part are omitted, or hostport=0 or empty, then 127.0.0.1 is will be used.
                * If no port is specified, the the default port is assumed and must be specified.
                * @param hostport the host-port combination
                * @param defPort the default service, if hostport does not contain a port number
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ::std::exception if some kind of error occurred
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                * @throws ::std::invalid_argument if hostport==0 || strlen(hostport)==0
                */
            public:
               static Endpoint createEndpoint(const char* hostport, ::std::uint32_t defPort, SocketType t =
                     SocketType(), Domain d = Domain(), Protocol p = Protocol());

               /**
                * Create an endpoint from a host-port string. The host-port string is a string
                * of the form @code [<node|address>]:<port> @endcode. This constructor
                * simplifies the use of endpoints when using command line arguments.
                * If the node or address part are omitted, or hostport=0 or empty, then 127.0.0.1 is will be used.
                * @param hostport the host-port combination
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ::std::exception if some kind of error occurred
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                * @throws ::std::invalid_argument if hostport==0 || strlen(hostport)==0
                */
            public:
               static Endpoint createEndpoint(const char* hostport, SocketType t = SocketType(), Domain d = Domain(),
                     Protocol p = Protocol());

               /**
                * Create an end-point whose addresses represent the so-called ANY address. When
                * bound, those addresses will accept connections on any interface. For example,
                * assume that the host has two network cards, each one with a different IPv4 address.
                * If connections should be accepted from either network, then there are two options:
                * <ol>
                * <li>Create a sockets, each bound to one of the addresses
                * <li>Create a single socket, bound to the any addresss (0.0.0.0 in IPv4, or :: in IPv6)
                * </ol>
                * @param service the name of a service (can be 0)
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ::std::invalid_argument if node==0 && service==0
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                */
            public:
               static Endpoint createAnyEndpoint(const char* service, SocketType t = SocketType(), Domain d = Domain(),
                     Protocol p = Protocol());

               /**
                * Create an end-point whose addresses represent the so-called ANY address.
                * @param service the name of a service (can be 0)
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                */
            public:
               static Endpoint createAnyEndpoint(::std::uint32_t service, SocketType t = SocketType(), Domain d =
                     Domain(), Protocol p = Protocol());

               /**
                * Create an endpoint that is represented by the loopback address.
                * @param service a service (may not be 0)
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @return an endpoint whose addresses will be loopback addresses
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                */
            public:
               static Endpoint createLoopbackEndpoint(const char* service, SocketType t = SocketType(), Domain d =
                     Domain(), Protocol p = Protocol());

               /**
                * Create an endpoint that is represented by the loopback address.
                * @param service a service
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @return an endpoint whose addresses will be loopback addresses
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                */
            public:
               static Endpoint createLoopbackEndpoint(::std::uint32_t service, SocketType t = SocketType(), Domain d =
                     Domain(), Protocol p = Protocol());

               /**
                * Select only the address that match the given parameters
                * @param t a socket type to filter on
                * @param d the domain to filter on
                * @param p the protocol to filter on
                * @return a new endpoint
                */
            public:
               Endpoint filter(SocketType t = SocketType(), Domain d = Domain(), Protocol p = Protocol()) const;

               /**
                * Get the first address associated with this endpoint. In most cases,
                * callers should loop over all addresses known to this endpoint.
                * @return *begin()
                * @throws ::std::exception if addressCount() == 0
                */
            public:
               Address address() const;

               /**
                * Get the number of addresses by which this endpoint can be addressed.
                * @return the number of addresses
                */
            public:
               inline size_t addressCount() const
               {
                  return _addresses.size();
               }

               /**
                * Test if this endpoint has no addresses.
                * @return addressCount()==0
                */
            public:
               inline bool empty() const
               {
                  return _addresses.empty();
               }

               /**
                * Get the first address.
                * @return an iterator to the first address for this endpoint
                */
            public:
               inline const_iterator begin() const
               {
                  return _addresses.begin();
               }

               /**
                * Get the iterator representing the end of an iteration.
                * @return the end iterator
                */
            public:
               inline const_iterator end() const
               {
                  return _addresses.end();
               }

               /**
                * Bind a socket to the first address defined by this end-point.
                * @param s a socket
                * @exception SocketException if the socket could not be bound to the end-point
                */
            public:
               void bind(::canopy::io::net::Socket& s) const;

               /**
                * Connect a socket to the first address defined by this end-point.
                * @param s a socket
                * @exception SocketException if the socket could not be bound to the end-point
                */
            public:
               void connect(::canopy::io::net::Socket& s) const;

               /**
                * Create a socket that is bound this end-point. This will bind
                * the socket to the <em>first</em> address known to the end-point.
                * Currently, this method does not allow setting of generic socket options
                * before binding.
                *
                * @note Use getBoundAddress() to obtain the address used for binding.
                *
                * @param reuseAddr reuse an address if possible (if true)
                * @return a socket that was bound to an address defined by the end-point.
                * @exception SocketException if the socket could not be bound
                * to the address and port
                */
            public:
               ::std::unique_ptr< ::canopy::io::net::Socket> createBoundSocket(bool reuseAddr = true) const;

               /**
                * Create a socket that will connect this endpoint.
                * @return a connected socket
                * @exception ConnectionRefused if the connection failed
                */
            public:
               ::std::unique_ptr< ::canopy::io::net::Socket> createConnectedSocket() const;

               /**
                * Create an Endpoint for a node and service. The endpoint should be
                * reachable via a socket in the specified domain and type.
                * @param node the name of a node (optional can be 0)
                * @param service the name of a service (can be 0)
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @param bindable true if the addresses should be bindable
                * @throws ::std::exception if some kind of error occurred
                * @throws AdressException if there is no INET_4 or INET_6 host
                * @throws ::std::invalid_argument if node==0 && service==0
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                */
            private:
               void lookupAddresses(const char* node, const char* service, SocketType t, Domain d, Protocol p,
                     bool bindable);

               /**
                * Create an Endpoint for a node and service. The endpoint should be
                * reachable via a socket in the specified domain and type.
                * @param node the name of a node (optional can be 0)
                * @param service the name of a service
                * @param d the address domain in which the address will be used
                * @param t the socket type used to contact the address
                * @param p the protocol that will be used to communicate with the entity at the address
                * @param bindable true if the addresses should be bindable
                * @throws ::std::exception if some kind of error occurred
                * @throws AdressException if there is no INET_4 or INET_6 host
                * @throws ::std::invalid_argument if node==0 && service==0
                * @throws ResourceUnavailable if the endpoint cannot be lookup at the momement
                */
            private:
               void lookupAddresses(const char* node, ::std::uint32_t service, SocketType t, Domain d, Protocol p,
                     bool bindable);

               /** The addresses for this endpoint */
            private:
               ::std::vector< Address> _addresses;
         };
      }
   }
}

/**
 * Print some representation of this address
 * @param out a stream
 * @param addr an address
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::canopy::io::net::Endpoint& ep);

#endif

