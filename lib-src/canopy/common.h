#ifndef _CANOPY_COMMON_H
#define _CANOPY_COMMON_H

#include <cstdint>
#include <algorithm>
#include <cerrno>
#include <cstring>
#include <functional>
#include <iostream>
#include <string>

namespace canopy {


   class Finally
   {
         Finally() = delete;
         Finally(const Finally&) = delete;
         Finally&operator=(const Finally&) = delete;
      public:
         inline ~Finally()
         {
            if (_finally) {
               _finally();
            }

         }
         inline Finally(const ::std::function< void()>& f)
               : _finally(f)
         {
         }

         inline Finally(Finally&& f)
               : _finally(::std::move(f._finally))
         {
         }
         inline Finally&operator=(Finally&& f)
         {
            _finally = ::std::move(f._finally);
            return *this;
         }

      private:
         ::std::function< void()> _finally;
   };

   /**
    * Count the number of bits in an integer
    * @param x a value
    * @return the number of 1 bits set in x
    */
#if CANOPY_COMPILER == CANOPY_GCC
   inline size_t countOnes(::std::uint32_t x)
   {
      return __builtin_popcount(x);
   }
#else
   size_t countOnes(::std::uint32_t x);
#endif

   /**
    * Count the number of bits in an integer
    * @param x a value
    * @return the number of 1 bits set in x
    */
#if CANOPY_COMPILER == CANOPY_GCC
   inline size_t countOnes(::std::uint64_t x)
   {
      return __builtin_popcountl(x);
   }
#else
size_t countOnes(::std::uint64_t x);
#endif

}
#endif
