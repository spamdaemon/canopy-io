#include <canopy/io/Selector.h>
#include <canopy/io/net/Endpoint.h>
#include <canopy/io/net/Network.h>
#include <canopy/io/net/PosixSocket.h>
#include <iostream>
#include <cassert>
#include <map>
#include <thread>
#include <chrono>
#include <chrono>

using namespace ::std;
using namespace ::chrono_literals;
using namespace ::canopy::io;
using namespace ::canopy::io::net;

using  canopy::io::net::PosixSocket;
typedef ::std::shared_ptr<PosixSocket> SockType;

static ::std::string getBroadcastAddr()
{
   auto networks = Network::getNetworks(INET_4, Network::BROADCAST | Network::RUNNING | Network::UP);
   ::std::string res;
   for (auto nw : networks) {
      if (nw.endpoint().address().nodeID() != nw.getBroadcastAddress().address().nodeID()) {
         res = nw.getBroadcastAddress().address().nodeID();
         if (!nw.testFlags(Network::LOOPBACK)) {
            // not a loop-back device
            break;
         }
      }
   }
   if (res.empty()) {
      throw NetException("No suitable broadcast address found");
   }
   return res;
}

void testSelector()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   ::std::string bcastAddr = getBroadcastAddr();
   if (bcastAddr.empty()) {
      ::std::cerr << __FUNCTION__ << ": cannot test selected since broadcast address could not be determined"
            << ::std::endl;
      return;
   }

   const char* ipaddr = bcastAddr.c_str();

   SockType dummy(new PosixSocket(DATAGRAM, INET_4));
   SockType receiver(new PosixSocket(DATAGRAM, INET_4));
   SockType sender(new PosixSocket(DATAGRAM, INET_4));
   cerr << __LINE__ << endl;
   Selector selector;
   sender->setBroadcastEnabled(true);

   cerr << __LINE__ << endl;
   receiver->bind(*(Endpoint(ipaddr, "54321", DATAGRAM, INET_4).begin()));

   cerr << __LINE__ << endl;
   assert(selector.size() == 0);
   selector.add(receiver, ::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE);
   assert(selector.getRegisteredEvents(receiver) == (::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE));
   selector.remove(receiver, ::canopy::io::IOEvents::WRITE);
   assert(selector.getRegisteredEvents(receiver) == ::canopy::io::IOEvents::READ);
   selector.remove(receiver, ::canopy::io::IOEvents::READ);
   assert(selector.size() == 0);
   selector.add(receiver, ::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE);
   selector.add(sender, ::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE);
   selector.add(sender, ::canopy::io::IOEvents::READ | ::canopy::io::IOEvents::WRITE);
   assert(selector.size() == 2);
   selector.setAutoWakeupEnabled(!selector.isAutoWakeupEnabled());
   selector.wakeup();
   selector.clearPendingWakeup();
   assert(selector.isSelectable(receiver));
   assert(selector.isSelectable(sender));
   assert(!selector.isSelectable(dummy));

   cerr << "Selecting " << endl;
   Selector::Events events = selector.select(makeTimeout(Timeout(100)),Selector::Events());
   assert(events.size() == 2);
   for (Selector::Events::iterator i = events.begin(); i != events.end(); ++i) {
      assert(i->second == ::canopy::io::IOEvents::WRITE);
   }

   const string hello("Hello, World");
   sender->writeTo(hello.c_str(), hello.length() + 1, *(Endpoint(ipaddr, "54321", DATAGRAM, INET_4).begin()),Timeout(1));

   cerr << "Sleeping " << endl;
   this_thread::sleep_for(::std::chrono::seconds(1));
   cerr << "Selecting " << endl;
   events = selector.select(makeTimeout(::std::chrono::microseconds(1)), ::std::move(events));
   assert(events.size() == 2);
   for (Selector::Events::iterator i = events.begin(); i != events.end(); ++i) {
      if (i->first == receiver) {
         ::std::cerr << "Receiver has events : " << i->second << ::std::endl;
      }
      else {
         ::std::cerr << "Sender has events : " << i->second << ::std::endl;
      }
   }
   for (Selector::Events::iterator i = events.begin(); i != events.end(); ++i) {
      if (i->first == receiver) {
         assert((i->second & ::canopy::io::IOEvents::READ) == ::canopy::io::IOEvents::READ);
      }
      assert((i->second & ::canopy::io::IOEvents::WRITE) == ::canopy::io::IOEvents::WRITE);
   }

   assert(
         Selector::select(receiver, ::canopy::io::IOEvents::WRITE | ::canopy::io::IOEvents::READ, Timeout(1))
               == (::canopy::io::IOEvents::WRITE | ::canopy::io::IOEvents::READ));

}

void testSelection()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   Endpoint xaddr("localhost", 54321, DATAGRAM, INET_4);
   Selector selector;
   SockType sock(new PosixSocket(DATAGRAM, INET_4));
   sock->bind(*xaddr.begin());
   selector.add(sock, ::canopy::io::IOEvents::READ);
   Selector::Events events;
   events = selector.select(Timeout::zero(), ::std::move(events));

   auto then = ::chrono::steady_clock::now();
   for (int i = 0; i < 10; ++i) {
      events = selector.select(makeTimeout(::std::chrono::milliseconds(100)), ::std::move(events));
   }
   auto now = ::chrono::steady_clock::now();
   cerr << "Delta time : " << (now - then).count() << endl;

   assert(now-then > 1000ms);
   assert(now-then < 1100ms);
}

int main()
{
   cerr << "Make sure the firewall is disabled if this hangs" << endl;

   try {
      testSelector();
      testSelection();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
