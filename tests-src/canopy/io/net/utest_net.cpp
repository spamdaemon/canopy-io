#include <canopy/io/net/Endpoint.h>
#include <iostream>
#include <cassert>
#include <chrono>
#include <thread>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::io::net;

void testParseHostPort()
{
   const HostPort def("foo", 19);
   auto hp = parseHostPort("127.0.0.1:13");
   assert(hp.host == "127.0.0.1");
   assert(hp.port == 13);

   hp = parseHostPort("[::1]:9090");
   assert(hp.host == "[::1]");
   assert(hp.port == 9090);

   try {
      // invalid port
      hp = parseHostPort("[::1]:foo");
      assert(false);
   }
   catch (const ::std::invalid_argument&) {
   }

   try {
      // invalid port number
      hp = parseHostPort("[::1]:-1");
      assert(false);
   }
   catch (const ::std::invalid_argument&) {
   }

   hp = parseHostPort("", def);
   assert(hp.host == def.host);
   assert(hp.port == def.port);

   hp = parseHostPort(":", def);
   assert(hp.host == def.host);
   assert(hp.port == def.port);

   hp = parseHostPort(":17", def);
   assert(hp.host == def.host);
   assert(hp.port == 17);

   hp = parseHostPort("node:", def);
   assert(hp.host == "node");
   assert(hp.port == def.port);

   hp = parseHostPort("node", def);
   assert(hp.host == "node");
   assert(hp.port == def.port);

}

void testParseNodeService()
{
   const NodeService def("foo", "bar");

   auto ns = parseNodeService("127.0.0.1:13");
   assert(ns.node == "127.0.0.1");
   assert(ns.service == "13");

   ns = parseNodeService("[::1]:http");
   assert(ns.node == "[::1]");
   assert(ns.service == "http");

   ns = parseNodeService("", def);
   assert(ns.node == def.node);
   assert(ns.service == def.service);

   ns = parseNodeService(":", def);
   assert(ns.node == def.node);
   assert(ns.service == def.service);

   ns = parseNodeService(":service", def);
   assert(ns.node == def.node);
   assert(ns.service == "service");

   ns = parseNodeService("node:", def);
   assert(ns.node == "node");
   assert(ns.service == def.service);

   ns = parseNodeService("node", def);
   assert(ns.node == "node");
   assert(ns.service == def.service);

}

static void testDomainAddress()
{
   const Domain* ptr = &::canopy::io::net::PACKET;
   assert(ptr!=nullptr);
}

int main()
{
   try {
      testParseNodeService();
      testParseHostPort();
      testDomainAddress();
   }
   catch (const ::std::exception& e) {
      cerr << "Exception " << e.what() << endl;
      return 1;
   }

   return 0;
}
