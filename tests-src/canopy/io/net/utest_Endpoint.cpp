#include <canopy/io/net/Endpoint.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::io::net;

static void checkInvalidEndpoint(const string& ep)
{
   try {
      Endpoint::createEndpoint(ep.c_str(), 5000, SocketType(), Domain(), Protocol());
      assert("Expected an invalid endpoint" == 0);
   }
   catch (const ::std::exception& e) {
      ::std::cerr << "Got expected exception : " << e.what() << ::std::endl;
      // ok
   }
}

static void checkValidEndpoint(const string& ep)
{
   try {
      Endpoint::createEndpoint(ep.c_str(), 5000, SocketType(), Domain(), Protocol());
      ::std::cerr << "Got a valid port " << ep << ::std::endl;
   }
   catch (const ::std::exception& e) {
      ::std::cerr << "Got unexpected exception : " << e.what() << ::std::endl;
      assert("Expected a valid endpoint" == 0);
   }
}

static void print(const Address& addr, ::std::ostream& out)
{
   out << "\tNumeric : " << addr.nodeID() << "@" << addr.serviceID() << "\tName  : " << addr.nodeName() << "@"
         << addr.serviceName() << "\tdomain=" << addr.domain().id() << "\ttype=" << addr.type().id() << "\tprotocol="
         << addr.protocol().id();

}

static void print(const Endpoint& pt)
{

   for (Endpoint::const_iterator i = pt.begin(); i != pt.end(); ++i) {
      cerr << "Address" << ::std::endl;
      print(*i, cerr);
      cerr << endl;
   }
}

void test1()
{
   cerr << __FUNCTION__ << endl;
   print(Endpoint("google.com"));
   cerr << endl;
   print(Endpoint("localhost"));
   cerr << endl;
   print(Endpoint("localhost", 80));
   cerr << endl;
   print(Endpoint("::200:0:100:0", 80));
}

void testAnyEndpoint()
{
   cerr << __FUNCTION__ << endl;
   print(Endpoint::createAnyEndpoint("8000", STREAM));
   print(Endpoint::createAnyEndpoint(9000, STREAM));
}

void testInet4()
{
   cerr << __FUNCTION__ << endl;
   print(Endpoint("192.168.1.1", 8080));
   cerr << "Address:" << ::std::endl;
   print(Endpoint("192.168.1.1", 8080).address());
}

void testLoopbackEndpoint()
{
   cerr << __FUNCTION__ << endl;
   print(Endpoint::createLoopbackEndpoint(8000, STREAM));
   print(Endpoint::createLoopbackEndpoint("9000", STREAM));
}

void testInvalidPort()
{
   cerr << __FUNCTION__ << endl;
   checkInvalidEndpoint("google.com:-1");
   checkInvalidEndpoint("google.com:1a");
   checkInvalidEndpoint("google.com:a");
}

void testValidPort()
{
   cerr << __FUNCTION__ << endl;
   checkValidEndpoint("google.com:1");
   checkValidEndpoint("google.com:1324");
   checkValidEndpoint("google.com:");
   checkValidEndpoint(":");
   checkValidEndpoint(":5");
}

int main()
{
   try {
      test1();
      testInet4();
      testAnyEndpoint();
      testLoopbackEndpoint();
      testValidPort();
      testInvalidPort();
   }
   catch (const ::std::exception& e) {
      cerr << "Exception " << e.what() << endl;
      return 1;
   }

   return 0;
}
