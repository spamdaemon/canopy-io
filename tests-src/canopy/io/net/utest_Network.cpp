#include <canopy/io/net/Network.h>
#include <iostream>
#include <set>
#include <string>
#include <cassert>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::io::net;

static vector< Network> listNetworks(Domain d = Domain(), uint32_t flags = 0)
{
   vector< Network> res;
   Network::enumerateInterfaces([&res] (const Network& net) {res.push_back(net);}, d, flags);
   return res;
}

void test_listNetworks()
{
   cerr << __FUNCTION__ << endl;
   auto nets = listNetworks();
   assert(!nets.empty());
   for (const Network& net : nets) {
      ::std::cerr << "Network " << net << ::std::endl;
   }
}

void test_listInetNetworks()
{
   cerr << __FUNCTION__ << endl;
   size_t n = 0;
   for (auto net : listNetworks()) {
      if (net.endpoint().address().domain() == INET_4) {
         ++n;
      }
   }
   if (n == 0) {
      ::std::cerr << "No result" << ::std::endl;
   }
   auto allInetNets = listNetworks(INET_4);
   assert(allInetNets.size() == n);
}

void test_listMulticastNetworks()
{
   cerr << __FUNCTION__ << endl;
   size_t n = 0;
   for (auto net : listNetworks()) {
      if (net.testFlags(Network::MULTICAST)) {
         ++n;
      }
   }
   if (n == 0) {
      ::std::cerr << "No result" << ::std::endl;
   }
   auto allMulticastNets = listNetworks(Domain(), Network::MULTICAST);
   assert(allMulticastNets.size() == n);
}

void test_listNetworkNames()
{
   cerr << __FUNCTION__ << endl;
   set< string> names;
   for (auto net : listNetworks(Domain(), Network::MULTICAST)) {
      names.insert(net.interface());
   }
   if (names.empty()) {
      ::std::cerr << "No result" << ::std::endl;
   }
   auto nets = Network::getNetworkNames(Domain(), Network::MULTICAST);
   for (auto net : nets) {
      ::std::cerr << "Network " << net << ::std::endl;
      assert(names.count(net) != 0);
   }
   assert(nets.size() == names.size());
}

void test_getNetworkByName()
{
   cerr << __FUNCTION__ << endl;
   auto nw = Network::getNetworks(INET_4, Network::MULTICAST);
   if (nw.empty()) {
      ::std::cerr << "No result" << ::std::endl;
      return;
   }
   auto net1 = nw[0];
   auto net2 = Network::getNetworkByName(net1.interface(), net1.domain());
   assert(net1.endpoint().address().nodeID()==net2.endpoint().address().nodeID());

}

void test_getUnknownNetworkByName()
{
   cerr << __FUNCTION__ << endl;
   try {
      Network::getNetworkByName("foobar",Domain(0));
      assert(false);
   }
   catch (const ::std::exception& e) {

   }
}

int main()
{
   try {
      test_listNetworks();
      test_listInetNetworks();
      test_listMulticastNetworks();
      test_listNetworkNames();
      test_getNetworkByName();
      test_getUnknownNetworkByName();
   }
   catch (const ::std::exception& e) {
      cerr << "Exception " << e.what() << endl;
      return 1;
   }

   return 0;
}
