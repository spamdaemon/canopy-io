#include <canopy/io/net/Endpoint.h>
#include <canopy/io/net/PosixSocket.h>
#include <canopy/io/net/Socket.h>
#include <iostream>
#include <cassert>
#include <map>
#include <thread>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::io::net;

using  canopy::io::net::PosixSocket;
typedef ::std::shared_ptr<PosixSocket> SockType;

void testSocketEquality()
{

   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   // test that socket equality is invariant under close()
   map< SockType, int> sockets;
   SockType s1(new PosixSocket(DATAGRAM, INET_4));
   SockType s2(new PosixSocket(DATAGRAM, INET_4));
   SockType s3(new PosixSocket(DATAGRAM, INET_4));

   sockets.insert(map< SockType, int>::value_type(s1, 1));
   sockets.insert(map< SockType, int>::value_type(s2, 2));
   sockets.insert(map< SockType, int>::value_type(s3, 3));

   assert(sockets.find(s1) != sockets.end());
   assert(sockets.find(s1) != sockets.end());
   assert(sockets.find(s1) != sockets.end());

   s1->close();
   s2->close();
   s3->close();

   assert(sockets.find(s1) != sockets.end());
   assert(sockets.find(s1) != sockets.end());
   assert(sockets.find(s1) != sockets.end());

}

void testConnectEndpoint()
{
   ::std::cerr << "TESTING : " << __FUNCTION__ << ::std::endl;
   auto s = Endpoint("google.com", "http").filter(STREAM).createConnectedSocket();

   Address peer(s->getPeerAddress());
   cerr << "Connected to " << peer.nodeName() << " at " << peer.serviceName() << endl;

   try {
      auto tmo = Endpoint().createConnectedSocket();
      assert(false);
   }
   catch (const exception& e) {
      cerr << __FUNCTION__ << ":" << e.what() << endl;
   }

}

int main()
{
   cerr << "Make sure the firewall is disabled if this hangs" << endl;

   try {

      testSocketEquality();
      testConnectEndpoint();
   }
   catch (const exception& e) {
      cerr << "Exception : " << e.what() << endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception" << endl;
      return 1;
   }
   return 0;
}
