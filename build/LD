#!/bin/bash

SCRIPT="$(readlink -e "$0")";
SCRIPT_DIR="$(dirname "$SCRIPT")";

message()
{ echo "$@" 1>&2; }

usage()
{
    message "$0 [-h] [-v] [-x <runtime-libs>] [-L <path>] [-l <library>] [-m <?>] <object> <executable>"
    message " Link an object with libraries into an executable"
    message "Options:"
    message " -h  this message";
    message " -x  the runtime libraries"
    message " -L  a library search path (use multiple times)";
    message " -l  a library for linking"
    message " -m  tbd"
    message " -v  increase verbosity";
    exit 1;
}

declare -a OPTS;
declare -a LIBS;
declare -a RUNTIME_LIBS;

RUNTIME=c

OPTIND=1;
while getopts "vhx:l:L:m:" opt; do
    case "$opt" in
	x)
	    RUNTIME="${OPTARG}";;
	L)
	    OPTS+=("-L${OPTARG}");;
	m)
	    OPTS+=("-m${OPTARG}");;
	l)
	    case "$(basename "${OPTARG}")" in
		lib*.a|lib*.a.*)
		    LIBS+=( "${OPTARG}" );;
		lib*.so|lib*.so.*)
		    LIBS+=( "${OPTARG}" );;
		*)
		    LIBS+=( "-l${OPTARG}" );
	    esac;;
	v)
	    VERBOSITY=$((VERBOSITY+1));;	    
	*)  usage;;
    esac;
done;
shift $((OPTIND-1));

OBJ="$1";
EXE="$2";
shift 2;

case "${RUNTIME}" in
    cxx)
	RUNTIME_LIBS+="-lstdc++";;
    c)
	true;;
    *)
	message "Unsupported runtime $RUNTIME";
	exit 1;;
esac;

declare -a EXEC;
EXEC=( gcc "${OPTS[@]}" -o "${EXE}" "${OBJ}" "${LIBS[@]}" "$@" "${RUNTIME_LIBS[@]}" )

if [ $VERBOSITY -gt 0 ]; then
    message "${EXEC[@]}";
fi;
exec "${EXEC[@]}";
